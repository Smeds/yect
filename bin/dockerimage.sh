#!/usr/bin/env bash

TARGET_BRANCH=${1}
DOCKERHUB_USER=${2}
DOCKERHUB_PASSWORD=${3}
REPO=${4}
DEST_BRANCH=${5}
BITBUCKET_BRANCH=${6}

if [ "${DEST_BRANCH}" = "${TARGET_BRANCH}" ]
then
    printf "Build image ${BITBUCKET_BRANCH}"
    npm install
    npm test
    docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASSWORD
    docker build -t ${REPO}:$BITBUCKET_BRANCH .
    docker push $REPO:$BITBUCKET_BRANCH
fi
