def get_encoding(file):
    import chardet
    return chardet.detect(open(file, "rb").read())['encoding']
