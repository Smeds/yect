import pandas as pd
from xlrd.biffh import XLRDError

from .data_record_extractors import Extractor
from .utils import get_encoding


def parse_file(file):
    encoding = get_encoding(file)
    if file.name.endswith(".csv"):
        return process_csv(file, encoding)
    elif file.name.endswith(".xls"):
        return process_xls(str(file), encoding)
    else:
        raise Exception("Unknown filetype %s!" % file.name)


def process_csv(file, encoding):
    data = pd.read_csv(file, encoding=encoding, sep=None, engine='python')
    extractor = Extractor("_".join(data.columns))
    return [(extractor.date(row), extractor.text(row), extractor.amount(row)) for index, row in data.iterrows()]


def process_xls(file, encoding):
    try:
        data = pd.read_excel(file, encoding=encoding, sep=None)
        counter = 0
        for row in data.itertuples():
            extractor = Extractor("_".join(str(r) for r in row[1:] if pd.notna(r)))
            counter += 1
            if extractor.mapper:
                break
        data = pd.read_excel(file, encoding=encoding, sep=None, skiprows=counter)
        data = data.dropna(how='all', axis='columns')
        extractor = Extractor("_".join(data.columns))
        if extractor.mapper is None:
            raise Exception("Couldn't parse provided excel file")
        return [(extractor.date(row), extractor.text(row), extractor.amount(row)) for index, row in data.iterrows()]
    except XLRDError:
        tables = pd.read_html(file, encoding=encoding, header=0, thousands="X")
        entries = []
        for t in tables:
            t = t.dropna(how='all', axis='columns')
            extractor = Extractor("_".join(t.columns))
            if extractor.mapper:
                entries.extend([(extractor.date(row), extractor.text(row), extractor.amount(row)) for index, row in t.iterrows()])
        return entries
