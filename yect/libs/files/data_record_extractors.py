import datetime
import re
from babel.numbers import parse_decimal

column = {'Datum_Text_Typ_Budgetgrupp_Belopp_Saldo': {"bank": 'ica', 'date': "Datum", 'text': 'Text', 'amount': 'Belopp'},
          'Reskontradatum_Transaktionsdatum_Text_Belopp_Saldo': {"bank": 'handelsbanken', 'date': "Transaktionsdatum",
                                                                 'text': 'Text', 'amount': 'Belopp'}}


class Extractor:
    def __init__(self, header_key):
        self.mapper = column.get(header_key, None)

    def date(self, data):
        return datetime.datetime.strptime(data.get(self.mapper['date']), "%Y-%m-%d")

    def text(self, data):
        return data.get(self.mapper['text'])

    def amount(self, data):
        return parse_decimal(re.sub('[A-Za-z€£$]', '', data.get(self.mapper['amount'])), locale='se')
