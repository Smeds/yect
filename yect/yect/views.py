from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from bankbook.views import add_account_for_menu


def index(request):
    context = add_account_for_menu(request)
    template = loader.get_template("base.html")
    return HttpResponse(template.render(context, request))
