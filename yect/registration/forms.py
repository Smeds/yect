from django import forms

from .models import RegistrationApplication


class RegistrationApplicationForm(forms.ModelForm):
    class Meta:
        model = RegistrationApplication
        fields = ['email']
        widgets = {'user': forms.EmailInput()}
        error_messages = {
            'email': {
                'unique': "The provided Email has already been used to apply for an account!",
            },
        }
