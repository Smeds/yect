from django.urls import path

from . import views

app_name = "registration"
urlpatterns = [
    path('apply', views.index, name='apply'),
    path('fail', views.fail, name='fail'),
]
