from django.http import HttpResponse
from django.template import loader

from .forms import RegistrationApplicationForm


def index(request):
    context = {}
    if request.method == 'POST':
        context['registration_form'] = RegistrationApplicationForm(request.POST)
        if context['registration_form'].is_valid():
            context['registration_form'].save()
    else:
        context['registration_form'] = RegistrationApplicationForm()
    template = loader.get_template("registration/registration_application.html")
    return HttpResponse(template.render(context, request))


def fail(request):
    template = loader.get_template("registration/failed_registration.html")
    return HttpResponse(template.render({}, request))
