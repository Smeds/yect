from .models import RegistrationApplication

USER_FIELDS = ['username', 'email']


def create_user(strategy, details, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}

    fields = dict((name, kwargs.get(name, details.get(name)))
                  for name in strategy.setting('USER_FIELDS', USER_FIELDS))

    registration = RegistrationApplication.get_user_application(fields['email'])

    if not fields:
        return

    if registration and len(registration) == 1 and registration[0].approved:
        return {
            'is_new': True,
            'user': strategy.create_user(**fields)
        }

    return
