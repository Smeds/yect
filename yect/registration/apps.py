from django.apps import AppConfig


class RegistrationApplicationConfig(AppConfig):
    name = 'registration'
