from django.db import models


class RegistrationApplication(models.Model):
    email = models.EmailField(max_length=254, unique=True)
    approved = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        status = "Approved" if self.approved else "waiting approval"
        return str(self.email) + ": " + status

    def get_user_application(email):
        return RegistrationApplication.objects.filter(email=email)
