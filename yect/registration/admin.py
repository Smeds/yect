from django.contrib import admin
from .models import RegistrationApplication


class RegistrationAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)


admin.site.register(RegistrationApplication, RegistrationAdmin)
