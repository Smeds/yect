from django.test import TestCase
from django.test import TestCase
from django.contrib.auth.models import User
from bankbook.models import Account, Bank, CombinedAccount, CombineItem, Statement, StatementType
from bankbook.models import StatementTypeMonthSummary, StatementTypeYearSummary
from bankbook.models import MonthSummaryCumulative, YearSummaryCumulative
from bankbook.models import CombinedStatementTypeMonthSummary, CombinedStatementTypeYearSummary
from bankbook.models import CombinedMonthSummaryCumulative, CombinedYearSummaryCumulative
from decimal import Decimal
from unittest import mock
import datetime


class SummaryTestsCase(TestCase):
    def setUp(self):
        mocked = datetime.datetime(2020, 7, 1, 0, 0, 0)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked)):
            bank1 = Bank.create_bank("bank1", "")
            bank2 = Bank.create_bank("bank2", "")

            user = User.objects.create(username='Testuser')

            self.account1 = Account.create_account(bank1, "123", "account1", 10, user)
            self.account2 = Account.create_account(bank2, "456", "account2", 20, user)
            self.account3 = Account.create_account(bank1, "789", "account3", 30, user)

            self.income = StatementType.create_statement_type("Income", "", None, 1)
            self.salary = StatementType.create_statement_type("Salary", "", self.income, 2)
            self.investment = StatementType.create_statement_type("Investment", "", self.income, 3)
            self.expenses = StatementType.create_statement_type("Expenses", "", None, 4)
            self.food = StatementType.create_statement_type("Food", "", self.expenses, 5)
            self.car = StatementType.create_statement_type("Car", "", self.expenses, 6)
            self.house = StatementType.create_statement_type("House", "", self.expenses, 7)
            self.operations = StatementType.create_statement_type("Operations", "", self.house, 8)

            self.combined_1 = CombinedAccount.create_account("Combined1", user)
            self.combined_1.add_combined_item(self.account1)
            self.combined_1.add_combined_item(self.account2)
            self.combined_1.create_sub_name()

            self.statement1_1 = Statement.create_statement(self.account1, -5,
                                                           datetime.datetime.strptime("2019-03-02", "%Y-%m-%d"), "Vatten",
                                                           self.house)
            self.statement1_2 = Statement.create_statement(self.account1, 11,
                                                           datetime.datetime.strptime("2019-02-02", "%Y-%m-%d"), "Lön",
                                                           self.salary)
            self.statement1_3 = Statement.create_statement(self.account1, -6,
                                                           datetime.datetime.strptime("2019-04-02", "%Y-%m-%d"), "Vatten",
                                                           self.house)
            self.statement1_4 = Statement.create_statement(self.account1, 11,
                                                           datetime.datetime.strptime("2019-03-02", "%Y-%m-%d"), "Lön",
                                                           self.salary)
            self.statement1_5 = Statement.create_statement(self.account1, 12,
                                                           datetime.datetime.strptime("2019-03-22", "%Y-%m-%d"), "Investment",
                                                           self.investment)
            self.statement2_1 = Statement.create_statement(self.account2, -17,
                                                           datetime.datetime.strptime("2018-06-02", "%Y-%m-%d"), "Mat",
                                                           self.food)
            self.statement2_2 = Statement.create_statement(self.account2, -13,
                                                           datetime.datetime.strptime("2020-06-02", "%Y-%m-%d"), "Mat",
                                                           self.food)
            self.statement2_3 = Statement.create_statement(self.account2, -14,
                                                           datetime.datetime.strptime("2016-10-02", "%Y-%m-%d"), "Car",
                                                           self.car)
            self.statement2_4 = Statement.create_statement(self.account2, -10,
                                                           datetime.datetime.strptime("2019-03-02", "%Y-%m-%d"), "Car",
                                                           self.car)
            self.statement3_1 = Statement.create_statement(self.account3, -16,
                                                           datetime.datetime.strptime("2018-10-08", "%Y-%m-%d"), "Car",
                                                           self.car)
            self.statement3_2 = Statement.create_statement(self.account3, -18,
                                                           datetime.datetime.strptime("2017-10-08", "%Y-%m-%d"), "Car",
                                                           self.car)
            self.statement3_3 = Statement.create_statement(self.account3, -19,
                                                           datetime.datetime.strptime("2019-10-08", "%Y-%m-%d"), "Car",
                                                           self.car)

            self.combined_2 = CombinedAccount.create_account("Combined2", user)
            self.combined_2.add_combined_item(self.account1)
            self.combined_2.add_combined_item(self.account2)
            self.combined_2.create_sub_name()

            self.account1_1 = Account.get_account_by_id(self.account1.pk)
            self.account1_1.name = "account1_1"
            self.account1_1.save()

            self.account1.start_balance = 11
            self.account1.save()

    def test_month_summary(self):
        self.month_summary()

    def month_summary(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account1).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 9)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account1, Decimal(11), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account1, Decimal(11), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account1, Decimal(23), self.income))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account1, Decimal(11), self.salary))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account1, Decimal(12), self.investment))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account1, Decimal(-5), self.expenses))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.account1, Decimal(-5), self.house))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.account1, Decimal(-6), self.expenses))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.account1, Decimal(-6), self.house))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account2).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 8)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account2, Decimal(-10), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account2, Decimal(-10), self.car))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.account2, Decimal(-13), self.expenses))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.account2, Decimal(-13), self.food))

        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account3).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 6)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account3, Decimal(-18), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account3, Decimal(-18), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account3, Decimal(-16), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account3, Decimal(-16), self.car))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account3, Decimal(-19), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account3, Decimal(-19), self.car))

        """
            self.account2, -14, 2016-10-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account2, -10, 2019-03-02, Car, self.car
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedStatementTypeMonthSummary.objects.filter(account=self.combined_1).order_by("year", "month",
                                                                                                     "statement_type")
        self.assertEqual(len(entries), 16)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_1, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_1, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_1, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_1, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_1, Decimal(11), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_1, Decimal(11), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_1, Decimal(23), self.income))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_1, Decimal(11), self.salary))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_1, Decimal(12), self.investment))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_1, Decimal(-15), self.expenses))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_1, Decimal(-10), self.car))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_1, Decimal(-5), self.house))
        self.assertEqual((entries[12].account, entries[12].balance, entries[12].statement_type),
                         (self.combined_1, Decimal(-6), self.expenses))
        self.assertEqual((entries[13].account, entries[13].balance, entries[13].statement_type),
                         (self.combined_1, Decimal(-6), self.house))
        self.assertEqual((entries[14].account, entries[14].balance, entries[14].statement_type),
                         (self.combined_1, Decimal(-13), self.expenses))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].statement_type),
                         (self.combined_1, Decimal(-13), self.food))

        entries = CombinedStatementTypeMonthSummary.objects.filter(account=self.combined_2).order_by("year", "month",
                                                                                                     "statement_type")
        self.assertEqual(len(entries), 16)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_2, Decimal(11), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_2, Decimal(11), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_2, Decimal(23), self.income))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_2, Decimal(11), self.salary))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_2, Decimal(12), self.investment))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_2, Decimal(-15), self.expenses))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_2, Decimal(-10), self.car))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_2, Decimal(-5), self.house))
        self.assertEqual((entries[12].account, entries[12].balance, entries[12].statement_type),
                         (self.combined_2, Decimal(-6), self.expenses))
        self.assertEqual((entries[13].account, entries[13].balance, entries[13].statement_type),
                         (self.combined_2, Decimal(-6), self.house))
        self.assertEqual((entries[14].account, entries[14].balance, entries[14].statement_type),
                         (self.combined_2, Decimal(-13), self.expenses))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].statement_type),
                         (self.combined_2, Decimal(-13), self.food))

    def month_summary_delete_test(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            ##self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account1).order_by("year", "month", "statement_type")

        self.assertEqual(len(entries), 7)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account1, Decimal(11), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account1, Decimal(11), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account1, Decimal(23), self.income))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account1, Decimal(11), self.salary))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account1, Decimal(12), self.investment))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account1, Decimal(-6), self.expenses))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.account1, Decimal(-6), self.house))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account2).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 0)

        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account3).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 0)

        """
            ##self.account2, -14, 2016-10-02, Car, self.car
            ##self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, 11, 2019-03-02, Lön, self.salary
            ##self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, 12, 2019-03-22, Investment, self.investment
            ##self.account2, -10, 2019-03-02, Car, self.car
            self.account1, -6, 2019-04-02, Vatten, self.house
            ##self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedStatementTypeMonthSummary.objects.filter(account=self.combined_1).order_by("year", "month",
                                                                                                     "statement_type")
        self.assertEqual(len(entries), 7)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_1, Decimal(11), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_1, Decimal(11), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_1, Decimal(23), self.income))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_1, Decimal(11), self.salary))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_1, Decimal(12), self.investment))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_1, Decimal(-6), self.expenses))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_1, Decimal(-6), self.house))

        entries = CombinedStatementTypeMonthSummary.objects.filter(account=self.combined_2).order_by("year", "month",
                                                                                                     "statement_type")
        self.assertEqual(len(entries), 7)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_2, Decimal(11), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_2, Decimal(11), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_2, Decimal(23), self.income))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_2, Decimal(11), self.salary))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_2, Decimal(12), self.investment))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_2, Decimal(-6), self.expenses))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_2, Decimal(-6), self.house))

    def test_month_summary_statement_change(self):
        self.statement1_1.type = self.operations
        self.statement1_1.save()
        """
           self.account1, 11, 2019-02-02, Lön, self.salary
           self.account1, -5, 2019-03-02, Vatten, self.operations
           self.account1, -6, 2019-04-02, Vatten, self.house
           self.account1, 11, 2019-03-02, Lön, self.salary
           self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account1).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 10)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account1, Decimal(11), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account1, Decimal(11), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account1, Decimal(23), self.income))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account1, Decimal(11), self.salary))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account1, Decimal(12), self.investment))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account1, Decimal(-5), self.expenses))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.account1, Decimal(-5), self.house))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.account1, Decimal(-5), self.operations))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.account1, Decimal(-6), self.expenses))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.account1, Decimal(-6), self.house))

        """
           self.account2, -13, 2020-06-02, Mat, self.food
           self.account2, -10, 2019-03-02, Car, self.car
           self.account2, -17, 2018-06-02, Mat, self.food
           self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account2).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 8)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account2, Decimal(-10), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account2, Decimal(-10), self.car))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.account2, Decimal(-13), self.expenses))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.account2, Decimal(-13), self.food))

        """
           self.account3, -18, 2017-10-08, Car, self.car
           self.account3, -16, 2018-10-08, Car, self.car
           self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = StatementTypeMonthSummary.objects.filter(account=self.account3).order_by("year", "month",
                                                                                           "statement_type")
        self.assertEqual(len(entries), 6)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account3, Decimal(-18), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account3, Decimal(-18), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account3, Decimal(-16), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account3, Decimal(-16), self.car))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account3, Decimal(-19), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account3, Decimal(-19), self.car))

        """
           self.account2, -14, 2016-10-02, Car, self.car
           self.account2, -17, 2018-06-02, Mat, self.food
           self.account1, 11, 2019-02-02, Lön, self.salary
           self.account1, 11, 2019-03-02, Lön, self.salary
           self.account1, -5, 2019-03-02, Vatten, self.operations
           self.account1, 12, 2019-03-22, Investment, self.investment
           self.account2, -10, 2019-03-02, Car, self.car
           self.account1, -6, 2019-04-02, Vatten, self.house
           self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedStatementTypeMonthSummary.objects.filter(account=self.combined_1).order_by("year", "month",
                                                                                                     "statement_type")
        self.assertEqual(len(entries), 17)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_1, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_1, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_1, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_1, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_1, Decimal(11), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_1, Decimal(11), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_1, Decimal(23), self.income))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_1, Decimal(11), self.salary))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_1, Decimal(12), self.investment))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_1, Decimal(-15), self.expenses))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_1, Decimal(-10), self.car))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_1, Decimal(-5), self.house))
        self.assertEqual((entries[12].account, entries[12].balance, entries[12].statement_type),
                         (self.combined_1, Decimal(-5), self.operations))
        self.assertEqual((entries[13].account, entries[13].balance, entries[13].statement_type),
                         (self.combined_1, Decimal(-6), self.expenses))
        self.assertEqual((entries[14].account, entries[14].balance, entries[14].statement_type),
                         (self.combined_1, Decimal(-6), self.house))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].statement_type),
                         (self.combined_1, Decimal(-13), self.expenses))
        self.assertEqual((entries[16].account, entries[16].balance, entries[16].statement_type),
                         (self.combined_1, Decimal(-13), self.food))

        entries = CombinedStatementTypeMonthSummary.objects.filter(account=self.combined_2).order_by("year", "month",
                                                                                                     "statement_type")
        self.assertEqual(len(entries), 17)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_2, Decimal(11), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_2, Decimal(11), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_2, Decimal(23), self.income))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_2, Decimal(11), self.salary))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_2, Decimal(12), self.investment))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_2, Decimal(-15), self.expenses))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_2, Decimal(-10), self.car))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_2, Decimal(-5), self.house))
        self.assertEqual((entries[12].account, entries[12].balance, entries[12].statement_type),
                         (self.combined_2, Decimal(-5), self.operations))
        self.assertEqual((entries[13].account, entries[13].balance, entries[13].statement_type),
                         (self.combined_2, Decimal(-6), self.expenses))
        self.assertEqual((entries[14].account, entries[14].balance, entries[14].statement_type),
                         (self.combined_2, Decimal(-6), self.house))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].statement_type),
                         (self.combined_2, Decimal(-13), self.expenses))
        self.assertEqual((entries[16].account, entries[16].balance, entries[16].statement_type),
                         (self.combined_2, Decimal(-13), self.food))

    def test_year_summary(self):
        self.year_summary()

    def year_summary(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account1).order_by("year", "statement_type")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account1, Decimal(34), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account1, Decimal(22), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account1, Decimal(12), self.investment))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account1, Decimal(-11), self.expenses))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account1, Decimal(-11), self.house))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account2).order_by("year", "statement_type")
        self.assertEqual(len(entries), 8)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account2, Decimal(-10), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account2, Decimal(-10), self.car))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.account2, Decimal(-13), self.expenses))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.account2, Decimal(-13), self.food))

        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account3).order_by("year", "statement_type")
        self.assertEqual(len(entries), 6)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account3, Decimal(-18), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account3, Decimal(-18), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account3, Decimal(-16), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account3, Decimal(-16), self.car))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account3, Decimal(-19), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account3, Decimal(-19), self.car))

        """
            self.account2, -14, 2016-10-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account2, -10, 2019-03-02, Car, self.car
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedStatementTypeYearSummary.objects.filter(account=self.combined_1).order_by("year", "statement_type")
        self.assertEqual(len(entries), 12)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_1, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_1, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_1, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_1, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_1, Decimal(34), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_1, Decimal(22), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_1, Decimal(12), self.investment))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_1, Decimal(-21), self.expenses))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_1, Decimal(-10), self.car))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_1, Decimal(-11), self.house))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_1, Decimal(-13), self.expenses))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_1, Decimal(-13), self.food))

        entries = CombinedStatementTypeYearSummary.objects.filter(account=self.combined_2).order_by("year",
                                                                                                    "statement_type")
        self.assertEqual(len(entries), 12)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_2, Decimal(34), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_2, Decimal(22), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_2, Decimal(12), self.investment))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_2, Decimal(-21), self.expenses))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_2, Decimal(-10), self.car))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_2, Decimal(-11), self.house))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_2, Decimal(-13), self.expenses))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_2, Decimal(-13), self.food))

    def year_summary_delete_test(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            ##self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account1).order_by("year", "statement_type")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account1, Decimal(34), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account1, Decimal(22), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account1, Decimal(12), self.investment))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account1, Decimal(-6), self.expenses))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account1, Decimal(-6), self.house))

        """
            ## self.account2, -13, 2020-06-02, Mat, self.food
            ## self.account2, -10, 2019-03-02, Car, self.car
            ## self.account2, -17, 2018-06-02, Mat, self.food
            ## self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account2).order_by("year", "statement_type")
        self.assertEqual(len(entries), 0)

        """
            ## self.account3, -18, 2017-10-08, Car, self.car
            ## self.account3, -16, 2018-10-08, Car, self.car
            ## self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account3).order_by("year", "statement_type")
        self.assertEqual(len(entries), 0)

        """
            ## self.account2, -14, 2016-10-02, Car, self.car
            ## self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            ##self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            ## self.account2, -10, 2019-03-02, Car, self.car
            self.account1, 12, 2019-03-22, Investment, self.investment
            ## self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedStatementTypeYearSummary.objects.filter(account=self.combined_1).order_by("year", "statement_type")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_1, Decimal(34), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_1, Decimal(22), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_1, Decimal(12), self.investment))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_1, Decimal(-6), self.expenses))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_1, Decimal(-6), self.house))

        entries = CombinedStatementTypeYearSummary.objects.filter(account=self.combined_2).order_by("year",
                                                                                                    "statement_type")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_2, Decimal(34), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_2, Decimal(22), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_2, Decimal(12), self.investment))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_2, Decimal(-6), self.expenses))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_2, Decimal(-6), self.house))

    def test_year_sumamary_statement_change(self):
        self.statement1_1.type = self.operations
        self.statement1_1.save()
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.operations
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account1).order_by("year", "statement_type")
        self.assertEqual(len(entries), 6)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account1, Decimal(34), self.income))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account1, Decimal(22), self.salary))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account1, Decimal(12), self.investment))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account1, Decimal(-11), self.expenses))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account1, Decimal(-11), self.house))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account1, Decimal(-5), self.operations))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account2).order_by("year", "statement_type")
        self.assertEqual(len(entries), 8)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account2, Decimal(-10), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account2, Decimal(-10), self.car))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.account2, Decimal(-13), self.expenses))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.account2, Decimal(-13), self.food))

        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = StatementTypeYearSummary.objects.filter(account=self.account3).order_by("year", "statement_type")
        self.assertEqual(len(entries), 6)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.account3, Decimal(-18), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.account3, Decimal(-18), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.account3, Decimal(-16), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.account3, Decimal(-16), self.car))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.account3, Decimal(-19), self.expenses))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.account3, Decimal(-19), self.car))

        """
            self.account2, -14, 2016-10-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.operations
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account2, -10, 2019-03-02, Car, self.car
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedStatementTypeYearSummary.objects.filter(account=self.combined_1).order_by("year", "statement_type")
        self.assertEqual(len(entries), 13)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_1, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_1, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_1, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_1, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_1, Decimal(34), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_1, Decimal(22), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_1, Decimal(12), self.investment))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_1, Decimal(-21), self.expenses))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_1, Decimal(-10), self.car))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_1, Decimal(-11), self.house))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_1, Decimal(-5), self.operations))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_1, Decimal(-13), self.expenses))
        self.assertEqual((entries[12].account, entries[12].balance, entries[12].statement_type),
                         (self.combined_1, Decimal(-13), self.food))

        entries = CombinedStatementTypeYearSummary.objects.filter(account=self.combined_2).order_by("year",
                                                                                                    "statement_type")
        self.assertEqual(len(entries), 13)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].statement_type),
                         (self.combined_2, Decimal(-14), self.expenses))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].statement_type),
                         (self.combined_2, Decimal(-14), self.car))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].statement_type),
                         (self.combined_2, Decimal(-17), self.expenses))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].statement_type),
                         (self.combined_2, Decimal(-17), self.food))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].statement_type),
                         (self.combined_2, Decimal(34), self.income))
        self.assertEqual((entries[5].account, entries[5].balance, entries[5].statement_type),
                         (self.combined_2, Decimal(22), self.salary))
        self.assertEqual((entries[6].account, entries[6].balance, entries[6].statement_type),
                         (self.combined_2, Decimal(12), self.investment))
        self.assertEqual((entries[7].account, entries[7].balance, entries[7].statement_type),
                         (self.combined_2, Decimal(-21), self.expenses))
        self.assertEqual((entries[8].account, entries[8].balance, entries[8].statement_type),
                         (self.combined_2, Decimal(-10), self.car))
        self.assertEqual((entries[9].account, entries[9].balance, entries[9].statement_type),
                         (self.combined_2, Decimal(-11), self.house))
        self.assertEqual((entries[10].account, entries[10].balance, entries[10].statement_type),
                         (self.combined_2, Decimal(-5), self.operations))
        self.assertEqual((entries[11].account, entries[11].balance, entries[11].statement_type),
                         (self.combined_2, Decimal(-13), self.expenses))
        self.assertEqual((entries[12].account, entries[12].balance, entries[12].statement_type),
                         (self.combined_2, Decimal(-13), self.food))

    def test_cum_month_summary(self):
        self.cum_month_summary()

    def cum_month_summary(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account1, -6, 2019-04-02, Vatten, self.house
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account1).order_by("year", "month")
        self.assertEqual(len(entries), 18)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account1, Decimal(22), 2019, 2))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year, entries[1].month),
                         (self.account1, Decimal(40), 2019, 3))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year, entries[2].month),
                         (self.account1, Decimal(34), 2019, 4))
        self.assertEqual((entries[17].account, entries[17].balance, entries[17].year, entries[17].month),
                         (self.account1, Decimal(34), 2020, 7))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account2).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account2, Decimal(6), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.account2, Decimal(6), 2017, 1))
        self.assertEqual((entries[19].account, entries[19].balance, entries[19].year, entries[19].month),
                         (self.account2, Decimal(6), 2018, 5))
        self.assertEqual((entries[20].account, entries[20].balance, entries[20].year, entries[20].month),
                         (self.account2, Decimal(-11), 2018, 6))
        self.assertEqual((entries[21].account, entries[21].balance, entries[21].year, entries[21].month),
                         (self.account2, Decimal(-11), 2018, 7))
        self.assertEqual((entries[28].account, entries[28].balance, entries[28].year, entries[28].month),
                         (self.account2, Decimal(-11), 2019, 2))
        self.assertEqual((entries[29].account, entries[29].balance, entries[29].year, entries[29].month),
                         (self.account2, Decimal(-21), 2019, 3))
        self.assertEqual((entries[30].account, entries[30].balance, entries[30].year, entries[30].month),
                         (self.account2, Decimal(-21), 2019, 4))
        self.assertEqual((entries[43].account, entries[43].balance, entries[43].year, entries[43].month),
                         (self.account2, Decimal(-21), 2020, 5))
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.account2, Decimal(-34), 2020, 7))
        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account3).order_by("year", "month")
        self.assertEqual(len(entries), 34)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account3, Decimal(12), 2017, 10))
        self.assertEqual((entries[24].account, entries[24].balance, entries[24].year, entries[24].month),
                         (self.account3, Decimal(-23), 2019, 10))
        self.assertEqual((entries[33].account, entries[33].balance, entries[33].year, entries[33].month),
                         (self.account3, Decimal(-23), 2020, 7))

        """
            self.account2, -14, 2016-10-02, Car, self.car 20 6
            self.account2, -17, 2018-06-02, Mat, self.food - 11
            self.account1, 11, 2019-02-02, Lön, self.salary 10 + 11  -11 = 10
            self.account1, -5, 2019-03-02, Vatten, self.house 5
            self.account1, -6, 2019-04-02, Vatten, self.house -1
            self.account1, 11, 2019-03-02, Lön, self.salary 10
            self.account1, 12, 2019-03-22, Investment, self.investment 22
            self.account2, -10, 2019-03-02, Car, self.car 12
            self.account2, -13, 2020-06-02, Mat, self.food -1
        """
        entries = CombinedMonthSummaryCumulative.objects.filter(account=self.combined_1).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.combined_1, Decimal(6), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.combined_1, Decimal(6), 2017, 1))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].year, entries[15].month),
                         (self.combined_1, Decimal(6), 2018, 1))
        self.assertEqual((entries[27].account, entries[27].balance, entries[27].year, entries[27].month),
                         (self.combined_1, Decimal(-11), 2019, 1))
        self.assertEqual((entries[39].account, entries[39].balance, entries[39].year, entries[39].month),
                         (self.combined_1, Decimal(13), 2020, 1))
        # ToDo Account updated, need to update combined
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.combined_1, Decimal(0), 2020, 7))

        entries = CombinedMonthSummaryCumulative.objects.filter(account=self.combined_2).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.combined_2, Decimal(6), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.combined_2, Decimal(6), 2017, 1))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].year, entries[15].month),
                         (self.combined_2, Decimal(6), 2018, 1))
        self.assertEqual((entries[27].account, entries[27].balance, entries[27].year, entries[27].month),
                         (self.combined_2, Decimal(-11), 2019, 1))
        self.assertEqual((entries[39].account, entries[39].balance, entries[39].year, entries[39].month),
                         (self.combined_2, Decimal(13), 2020, 1))
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.combined_2, Decimal(0), 2020, 7))

    def cum_month_summary_delete_test(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            ## self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account1, -6, 2019-04-02, Vatten, self.house
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account1).order_by("year", "month")
        self.assertEqual(len(entries), 18)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account1, Decimal(22), 2019, 2))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year, entries[1].month),
                         (self.account1, Decimal(45), 2019, 3))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year, entries[2].month),
                         (self.account1, Decimal(39), 2019, 4))
        self.assertEqual((entries[17].account, entries[17].balance, entries[17].year, entries[17].month),
                         (self.account1, Decimal(39), 2020, 7))

        """
            ## self.account2, -13, 2020-06-02, Mat, self.food
            ## self.account2, -10, 2019-03-02, Car, self.car
            ## self.account2, -17, 2018-06-02, Mat, self.food
            ## self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account2).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account2, Decimal(20), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.account2, Decimal(20), 2017, 1))
        self.assertEqual((entries[19].account, entries[19].balance, entries[19].year, entries[19].month),
                         (self.account2, Decimal(20), 2018, 5))
        self.assertEqual((entries[20].account, entries[20].balance, entries[20].year, entries[20].month),
                         (self.account2, Decimal(20), 2018, 6))
        self.assertEqual((entries[21].account, entries[21].balance, entries[21].year, entries[21].month),
                         (self.account2, Decimal(20), 2018, 7))
        self.assertEqual((entries[28].account, entries[28].balance, entries[28].year, entries[28].month),
                         (self.account2, Decimal(20), 2019, 2))
        self.assertEqual((entries[29].account, entries[29].balance, entries[29].year, entries[29].month),
                         (self.account2, Decimal(20), 2019, 3))
        self.assertEqual((entries[30].account, entries[30].balance, entries[30].year, entries[30].month),
                         (self.account2, Decimal(20), 2019, 4))
        self.assertEqual((entries[43].account, entries[43].balance, entries[43].year, entries[43].month),
                         (self.account2, Decimal(20), 2020, 5))
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.account2, Decimal(20), 2020, 7))
        """
            ## self.account3, -18, 2017-10-08, Car, self.car
            ## self.account3, -16, 2018-10-08, Car, self.car
            ## self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account3).order_by("year", "month")
        self.assertEqual(len(entries), 0)

        """
            ## self.account2, -14, 2016-10-02, Car, self.car
            ## self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
            ## self.account2, -10, 2019-03-02, Car, self.car
            ## self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedMonthSummaryCumulative.objects.filter(account=self.combined_1).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.combined_1, Decimal(20), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.combined_1, Decimal(20), 2017, 1))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].year, entries[15].month),
                         (self.combined_1, Decimal(20), 2018, 1))
        self.assertEqual((entries[27].account, entries[27].balance, entries[27].year, entries[27].month),
                         (self.combined_1, Decimal(20), 2019, 1))
        self.assertEqual((entries[39].account, entries[39].balance, entries[39].year, entries[39].month),
                         (self.combined_1, Decimal(59), 2020, 1))
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.combined_1, Decimal(59), 2020, 7))

        entries = CombinedMonthSummaryCumulative.objects.filter(account=self.combined_2).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.combined_2, Decimal(20), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.combined_2, Decimal(20), 2017, 1))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].year, entries[15].month),
                         (self.combined_2, Decimal(20), 2018, 1))
        self.assertEqual((entries[27].account, entries[27].balance, entries[27].year, entries[27].month),
                         (self.combined_2, Decimal(20), 2019, 1))
        self.assertEqual((entries[39].account, entries[39].balance, entries[39].year, entries[39].month),
                         (self.combined_2, Decimal(59), 2020, 1))
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.combined_2, Decimal(59), 2020, 7))

    def test_cum_month_statement_change(self):
        self.statement1_1.type = self.operations
        self.statement1_1.save()
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.operation
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account1, -6, 2019-04-02, Vatten, self.house
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account1).order_by("year", "month")
        self.assertEqual(len(entries), 18)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account1, Decimal(22), 2019, 2))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year, entries[1].month),
                         (self.account1, Decimal(40), 2019, 3))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year, entries[2].month),
                         (self.account1, Decimal(34), 2019, 4))
        self.assertEqual((entries[17].account, entries[17].balance, entries[17].year, entries[17].month),
                         (self.account1, Decimal(34), 2020, 7))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account2).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account2, Decimal(6), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.account2, Decimal(6), 2017, 1))
        self.assertEqual((entries[19].account, entries[19].balance, entries[19].year, entries[19].month),
                         (self.account2, Decimal(6), 2018, 5))
        self.assertEqual((entries[20].account, entries[20].balance, entries[20].year, entries[20].month),
                         (self.account2, Decimal(-11), 2018, 6))
        self.assertEqual((entries[21].account, entries[21].balance, entries[21].year, entries[21].month),
                         (self.account2, Decimal(-11), 2018, 7))
        self.assertEqual((entries[28].account, entries[28].balance, entries[28].year, entries[28].month),
                         (self.account2, Decimal(-11), 2019, 2))
        self.assertEqual((entries[29].account, entries[29].balance, entries[29].year, entries[29].month),
                         (self.account2, Decimal(-21), 2019, 3))
        self.assertEqual((entries[30].account, entries[30].balance, entries[30].year, entries[30].month),
                         (self.account2, Decimal(-21), 2019, 4))
        self.assertEqual((entries[43].account, entries[43].balance, entries[43].year, entries[43].month),
                         (self.account2, Decimal(-21), 2020, 5))
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.account2, Decimal(-34), 2020, 7))
        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = MonthSummaryCumulative.objects.filter(account=self.account3).order_by("year", "month")
        self.assertEqual(len(entries), 34)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.account3, Decimal(12), 2017, 10))
        self.assertEqual((entries[24].account, entries[24].balance, entries[24].year, entries[24].month),
                         (self.account3, Decimal(-23), 2019, 10))
        self.assertEqual((entries[33].account, entries[33].balance, entries[33].year, entries[33].month),
                         (self.account3, Decimal(-23), 2020, 7))

        """
            self.account2, -14, 2016-10-02, Car, self.car 20 6
            self.account2, -17, 2018-06-02, Mat, self.food - 11
            self.account1, 11, 2019-02-02, Lön, self.salary 10 + 11  -11 = 10
            self.account1, -5, 2019-03-02, Vatten, self.house 5
            self.account1, -6, 2019-04-02, Vatten, self.house -1
            self.account1, 11, 2019-03-02, Lön, self.salary 10
            self.account1, 12, 2019-03-22, Investment, self.investment 22
            self.account2, -10, 2019-03-02, Car, self.car 12
            self.account2, -13, 2020-06-02, Mat, self.food -1
        """
        entries = CombinedMonthSummaryCumulative.objects.filter(account=self.combined_1).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.combined_1, Decimal(6), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.combined_1, Decimal(6), 2017, 1))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].year, entries[15].month),
                         (self.combined_1, Decimal(6), 2018, 1))
        self.assertEqual((entries[27].account, entries[27].balance, entries[27].year, entries[27].month),
                         (self.combined_1, Decimal(-11), 2019, 1))
        self.assertEqual((entries[39].account, entries[39].balance, entries[39].year, entries[39].month),
                         (self.combined_1, Decimal(13), 2020, 1))
        # ToDo Account updated, need to update combined
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.combined_1, Decimal(0), 2020, 7))

        entries = CombinedMonthSummaryCumulative.objects.filter(account=self.combined_2).order_by("year", "month")
        self.assertEqual(len(entries), 46)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year, entries[0].month),
                         (self.combined_2, Decimal(6), 2016, 10))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year, entries[3].month),
                         (self.combined_2, Decimal(6), 2017, 1))
        self.assertEqual((entries[15].account, entries[15].balance, entries[15].year, entries[15].month),
                         (self.combined_2, Decimal(6), 2018, 1))
        self.assertEqual((entries[27].account, entries[27].balance, entries[27].year, entries[27].month),
                         (self.combined_2, Decimal(-11), 2019, 1))
        self.assertEqual((entries[39].account, entries[39].balance, entries[39].year, entries[39].month),
                         (self.combined_2, Decimal(13), 2020, 1))
        self.assertEqual((entries[45].account, entries[45].balance, entries[45].year, entries[45].month),
                         (self.combined_2, Decimal(0), 2020, 7))

    def test_cum_year_summary(self):
        self.cum_year_summary()

    def cum_year_summary(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account1).order_by("year")
        self.assertEqual(len(entries), 2)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account1, Decimal(34), 2019))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account2).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account2, Decimal(6), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.account2, Decimal(6), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.account2, Decimal(-11), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.account2, Decimal(-21), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year),
                         (self.account2, Decimal(-34), 2020))
        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account3).order_by("year")
        self.assertEqual(len(entries), 4)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account3, Decimal(12), 2017))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.account3, Decimal(-4), 2018))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.account3, Decimal(-23), 2019))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.account3, Decimal(-23), 2020))

        """
            self.account2, -14, 2016-10-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedYearSummaryCumulative.objects.filter(account=self.combined_1).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.combined_1, Decimal(6), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.combined_1, Decimal(6), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.combined_1, Decimal(-11), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.combined_1, Decimal(13), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year),
                         (self.combined_1, Decimal(0), 2020))

        entries = CombinedYearSummaryCumulative.objects.filter(account=self.combined_2).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.combined_2, Decimal(6), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.combined_2, Decimal(6), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.combined_2, Decimal(-11), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.combined_2, Decimal(13), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year,),
                         (self.combined_2, Decimal(0), 2020))

    def cum_year_summary_delete_test(self):
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            ## self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account1).order_by("year")
        self.assertEqual(len(entries), 2)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account1, Decimal(39), 2019))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.account1, Decimal(39), 2020))

        """
            ## self.account2, -13, 2020-06-02, Mat, self.food
            ## self.account2, -10, 2019-03-02, Car, self.car
            ## self.account2, -17, 2018-06-02, Mat, self.food
            ## self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account2).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account2, Decimal(20), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.account2, Decimal(20), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.account2, Decimal(20), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.account2, Decimal(20), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year),
                         (self.account2, Decimal(20), 2020))
        """
            ## self.account3, -18, 2017-10-08, Car, self.car
            ## self.account3, -16, 2018-10-08, Car, self.car
            ## self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account3).order_by("year")
        self.assertEqual(len(entries), 0)

        """
            ## self.account2, -14, 2016-10-02, Car, self.car
            ## self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            ##self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
            ## self.account2, -10, 2019-03-02, Car, self.car
            ## self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedYearSummaryCumulative.objects.filter(account=self.combined_1).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.combined_1, Decimal(20), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.combined_1, Decimal(20), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.combined_1, Decimal(20), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.combined_1, Decimal(59), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year),
                         (self.combined_1, Decimal(59), 2020))

        entries = CombinedYearSummaryCumulative.objects.filter(account=self.combined_2).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.combined_2, Decimal(20), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.combined_2, Decimal(20), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.combined_2, Decimal(20), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.combined_2, Decimal(59), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year,),
                         (self.combined_2, Decimal(59), 2020))

    def test_cum_year_summary_statement_change(self):
        self.statement1_1.type = self.operations
        self.statement1_1.save()
        """
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.operations
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account1).order_by("year")
        self.assertEqual(len(entries), 2)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account1, Decimal(34), 2019))

        """
            self.account2, -13, 2020-06-02, Mat, self.food
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account2, -14, 2016-10-02, Car, self.car
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account2).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account2, Decimal(6), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.account2, Decimal(6), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.account2, Decimal(-11), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.account2, Decimal(-21), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year),
                         (self.account2, Decimal(-34), 2020))
        """
            self.account3, -18, 2017-10-08, Car, self.car
            self.account3, -16, 2018-10-08, Car, self.car
            self.account3, -19, 2019-10-08, Car, self.car
        """
        entries = YearSummaryCumulative.objects.filter(account=self.account3).order_by("year")
        self.assertEqual(len(entries), 4)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.account3, Decimal(12), 2017))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.account3, Decimal(-4), 2018))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.account3, Decimal(-23), 2019))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.account3, Decimal(-23), 2020))

        """
            self.account2, -14, 2016-10-02, Car, self.car
            self.account2, -17, 2018-06-02, Mat, self.food
            self.account1, 11, 2019-02-02, Lön, self.salary
            self.account1, -5, 2019-03-02, Vatten, self.house
            self.account1, -6, 2019-04-02, Vatten, self.house
            self.account1, 11, 2019-03-02, Lön, self.salary
            self.account1, 12, 2019-03-22, Investment, self.investment
            self.account2, -10, 2019-03-02, Car, self.car
            self.account2, -13, 2020-06-02, Mat, self.food
        """
        entries = CombinedYearSummaryCumulative.objects.filter(account=self.combined_1).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.combined_1, Decimal(6), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.combined_1, Decimal(6), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.combined_1, Decimal(-11), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.combined_1, Decimal(13), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year),
                         (self.combined_1, Decimal(0), 2020))

        entries = CombinedYearSummaryCumulative.objects.filter(account=self.combined_2).order_by("year")
        self.assertEqual(len(entries), 5)
        self.assertEqual((entries[0].account, entries[0].balance, entries[0].year),
                         (self.combined_2, Decimal(6), 2016))
        self.assertEqual((entries[1].account, entries[1].balance, entries[1].year),
                         (self.combined_2, Decimal(6), 2017))
        self.assertEqual((entries[2].account, entries[2].balance, entries[2].year),
                         (self.combined_2, Decimal(-11), 2018))
        self.assertEqual((entries[3].account, entries[3].balance, entries[3].year),
                         (self.combined_2, Decimal(13), 2019))
        self.assertEqual((entries[4].account, entries[4].balance, entries[4].year,),
                         (self.combined_2, Decimal(0), 2020))

    def test_data_structure_generators(self):
        from .utils.dataStructurGenerators import stacked_area_chart
        stacked_area = stacked_area_chart(MonthSummaryCumulative.get_statement_summary_cumulative([self.account2]))
        self.assertEqual(len(stacked_area[0]['values']), 46)
        self.assertEqual(stacked_area[0]['values'][0], [1475280000000.0, 6.0])
        self.assertEqual(stacked_area[0]['values'][3], [1483228800000.0, 6.0])
        self.assertEqual(stacked_area[0]['values'][19], [1525132800000.0, 6.0])
        self.assertEqual(stacked_area[0]['values'][20], [1527811200000.0, -11.0])
        self.assertEqual(stacked_area[0]['values'][21], [1530403200000.0, -11.0])
        self.assertEqual(stacked_area[0]['values'][28], [1548979200000.0, -11.0])
        self.assertEqual(stacked_area[0]['values'][29], [1551398400000.0, -21.0])
        self.assertEqual(stacked_area[0]['values'][30], [1554076800000.0, -21.0])
        self.assertEqual(stacked_area[0]['values'][43], [1588291200000.0, -21.0])
        self.assertEqual(stacked_area[0]['values'][44], [1590969600000.0, -34.0])

        stacked_area = stacked_area_chart(YearSummaryCumulative.get_statement_summary_cumulative([self.account2]), year=True)
        self.assertEqual(len(stacked_area[0]['values']), 5)
        self.assertEqual(stacked_area[0]['values'][0], [1451606400000.0, 6.0])
        self.assertEqual(stacked_area[0]['values'][1], [1483228800000.0, 6.0])
        self.assertEqual(stacked_area[0]['values'][2], [1514764800000.0, -11.0])
        self.assertEqual(stacked_area[0]['values'][3], [1546300800000.0, -21.0])
        self.assertEqual(stacked_area[0]['values'][4], [1577836800000.0, -34.0])

    def test_combine_sub_name(self):
        self.assertEqual(self.combined_1.include_accounts_name,
                         ", ".join([self.account1.name, self.account2.name]))
        self.assertEqual(self.combined_2.include_accounts_name,
                         ", ".join([self.account1.name, self.account2.name]))

        self.assertEqual(CombinedAccount.objects.get(pk=self.combined_1.pk).include_accounts_name,
                         ", ".join([self.account1_1.name, self.account2.name]))
        self.assertEqual(CombinedAccount.objects.get(pk=self.combined_2.pk).include_accounts_name,
                         ", ".join([self.account1_1.name, self.account2.name]))

    def test_export_import_function(self):
        from .utils.data import export_statements_data, import_statements_data

        import tempfile
        with tempfile.TemporaryDirectory() as td:
            import os
            f_name = os.path.join(td, 'test')
            with open(f_name, 'w', encoding="utf8") as fh:
                for data in export_statements_data():
                    fh.write(data + "\n")
            self.account3.delete()
            for statement in Statement.objects.filter(account=self.account2):
                statement.delete()
            statements = Statement.objects.filter(account=self.account1)
            for statement in statements[0:2:len(statements)]:
                print("Delete: " + str(statement))
                statement.delete()
            import time
            time.sleep(5)

            self.month_summary_delete_test()
            self.year_summary_delete_test()
            self.cum_year_summary_delete_test()
            self.cum_month_summary_delete_test()

            with open(f_name, 'r', encoding="utf8") as fh:
                import_statements_data(fh)
            self.account3 = Account.objects.get(name="account3")

        self.month_summary()
        self.year_summary()
        self.cum_month_summary
        self.cum_year_summary
