from django.contrib.auth.models import User
from django.db import models, transaction
from django.db.models.signals import pre_save, post_save, post_delete, pre_delete
from django.dispatch import receiver
from django.db.models import F, Q
from django.utils.translation import gettext_lazy as _

from functools import reduce
import operator


class Bank(models.Model):
    name = models.CharField(max_length=200, help_text="Name of bank.")
    description = models.CharField(max_length=200, help_text="Description of bank.", blank=True, unique=False)

    def __str__(self):
        return self.name

    def create_bank(name, description):
        return Bank.objects.create(name=name, description=description)


class Account(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE, help_text="Bank")
    account_number = models.CharField(max_length=200, help_text="Account number")
    name = models.CharField(max_length=200, help_text="Name of account")
    start_balance = models.DecimalField(max_digits=19, decimal_places=2, help_text="Initial balance of the account")
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False, default=1)

    class Meta:
        unique_together = (('bank', 'account_number', 'owner'))

    def __init__(self, *args, **kwargs):
        super(Account, self).__init__(*args, **kwargs)
        self.__org_name = self.name
        self.__org_start_balance = self.start_balance

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.pk and (self.__org_name != self.name or self.__org_start_balance != self.start_balance):
            diff = self.start_balance - self.__org_start_balance
            if diff != 0:
                for summary in MonthSummaryCumulative.objects.filter(account__pk=self.pk):
                    summary.balance += diff
                    summary.excluded_balance += diff
                    summary.save()
                for summary in YearSummaryCumulative.objects.filter(account__pk=self.pk):
                    summary.balance += diff
                    summary.excluded_balance += diff
                    summary.save()
            for combine_item in CombineItem.objects.filter(account=self):
                combinedAccount = CombinedAccount.get_account_using_item(combine_item)
                if self.__org_name != self.name:
                    combinedAccount.create_sub_name()
            self.__org_name = self.name
            self.__org_start_balance = self.start_balance

    def __str__(self):
        return self.name + " : " + \
               self.bank.name + \
               " (" + self.account_number + " ) : " \
               + str(self.start_balance) + " : " + \
               self.owner.username

    def create_account(bank, account_number, name, start_balance, owner):
        return Account.objects.create(bank=bank,
                                      account_number=account_number,
                                      name=name,
                                      start_balance=start_balance,
                                      owner=owner)

    def get_accounts(user):
        return Account.objects.filter(owner=user).order_by('name')

    def get_account_by_id(id):
        return Account.objects.get(pk=id)

    def update_summaries(account, date):
        MonthSummaryCumulative.update_summaries(MonthSummaryCumulative, account, date)


class StatementType(models.Model):
    name = models.CharField(max_length=100, unique=False)
    description = models.CharField(max_length=300, unique=False)
    parent_type = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    sort_order = models.IntegerField(default=0, null=False, blank=False)

    def __str__(self):
        if self.parent_type is None or self.parent_type.pk == 20000 or self.parent_type.pk == 10000:
            return self.name
        else:
            return " -- " + self.name

    @staticmethod
    def create_statement_type(name, description, parent_type, sort_order):
        return StatementType.objects.create(name=name, description=description, parent_type=parent_type, sort_order=sort_order)

    @staticmethod
    def get_statement_types(names):
        return StatementType.objects.filter(reduce(operator.or_, [Q(name=n) for n in names]))

    @staticmethod
    def get_statement_types_by_id(id):
        return StatementType.objects.get(pk=id)


class Statement(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=19, decimal_places=2)
    exclude = models.BooleanField(default=False)
    date = models.DateField()
    text = models.CharField(max_length=200)
    type = models.ForeignKey(StatementType, on_delete=models.CASCADE, default=1)

    class Status(models.TextChoices):
        NEW = 'NEW', _('New')
        POSSIBLE_DUPLICATE = 'PD', _('Possible duplicate')
        SELF_SET = 'SF', _('Self set')
        AUTO_SET = 'AS', _('Auto Set')
        SKIP = 'SK', _('Skip')

    status = models.CharField(max_length=3, choices=Status.choices, default=Status.NEW)

    def __str__(self):
        return self.date.strftime("%Y-%m-%d") + ": " + str(self.amount) + " " + self.type.name + " " + self.status

    @transaction.atomic
    def update_summaries(account, date, amount, statement_type):
        while statement_type is not None:
            StatementTypeMonthSummary.update_month_summary(account=account, year=date.year, month=date.month,
                                                           statement_type=statement_type, amount=amount, exclude_amount=0)
            StatementTypeYearSummary.update_year_summary(account=account, year=date.year,
                                                         statement_type=statement_type, amount=amount, exclude_amount=0)

            statement_type = statement_type.parent_type

        recreate_month = MonthSummaryCumulative.update_month_summary(account, date.year, date.month, amount, 0)
        recreate_year = YearSummaryCumulative.update_year_summary(account, date.year, amount, 0)

    @transaction.atomic
    def insert_multiple_statements(account, statement_list):
        for (amount, date, text, statement_type) in statement_list:
            Statement.objects.create(account=account, amount=amount, date=date, text=text, type=statement_type)

    @transaction.atomic
    def create_statement(account, amount, date, text, statement_type):
        return Statement.objects.create(account=account, amount=amount, date=date, text=text, type=statement_type)

    @staticmethod
    def get_statements(accounts, year=None, month=None, day=None):
        if year is None and month is None and day is None:
            return Statement.objects.filter(reduce(operator.or_,
                                                   [Q(account=a)
                                                    for a in accounts])).order_by('-date', '-pk')
        if year is not None and month is None and day is None:
            return Statement.objects.filter(reduce(operator.or_,
                                                   [Q(date__year=year, account=a)
                                                    for a in accounts])).order_by('-date', '-pk')
        elif year is not None and month is not None and day is None:
            return Statement.objects.filter(reduce(operator.or_,
                                                   [Q(date__year=year, date__month=month, account=a)
                                                    for a in accounts])).order_by('-date', '-pk')
        elif year is not None and month is not None and day is not None:
            return Statement.objects.filter(reduce(operator.or_,
                                                   [Q(date__year=year, date__month=month, date__day=day, account=a)
                                                    for a in accounts])).order_by('-date', '-pk')
        else:
            raise Exception("Incorrect input")


class BaseMonthSummaryCumulative(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    year = models.IntegerField()
    month = models.IntegerField()
    balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    excluded_balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)

    class Meta:
        abstract = True
        unique_together = (('account', 'year', 'month'))

    def __str__(self):
        return str(self.year) + "-" + str(self.month) + ": " + str(self.balance)

    def create_month_summary_cumulative(summary_type, account, year, month, balance, excluded_balance):
        return summary_type.objects.create(account=account,
                                           year=year,
                                           month=month,
                                           balance=balance,
                                           excluded_balance=excluded_balance)

    def update_month_summary(summary_type, account, year, month, amount, exclude_amount):
        month_summary_cumulative, month_summary_cumulative_created = summary_type.objects.get_or_create(
            account=account, year=year, month=month,
            defaults={'account': account, 'year': year, 'month': month, 'balance': amount, 'excluded_balance': exclude_amount},
        )
        if not month_summary_cumulative_created:
            month_summary_cumulative.balance += amount
            month_summary_cumulative.save()
            next_entries = summary_type.objects.filter(
                reduce(operator.or_, [Q(account=account, year=year, month__gt=month),
                                      Q(account=account, year__gt=year)])).order_by('year', 'month')
            for entry in next_entries:
                entry.balance += amount
                entry.save()
            return False
        else:
            previous_entry = summary_type.objects.filter(
                reduce(operator.or_, [Q(account=account, year=year, month__lt=month),
                                      Q(account=account, year__lt=year)])).order_by('-year', '-month').first()
            if previous_entry is not None:
                if previous_entry.year == year:
                    dates = [(year, m) for m in range(previous_entry.month + 1, month)]
                else:
                    dates = [(previous_entry.year, m) for m in range(previous_entry.month + 1, 13)]
                    for y in range(previous_entry.year + 1, year):
                        dates += [(y, m) for m in range(1, 13)]
                    dates += [(year, m) for m in range(1, month)]
                for (y, m) in dates:
                    summary_type.create_month_summary_cumulative(account,
                                                                 y,
                                                                 m,
                                                                 previous_entry.balance,
                                                                 previous_entry.excluded_balance)
                month_summary_cumulative.balance += previous_entry.balance
                month_summary_cumulative.save()
            else:
                next_entries = summary_type.objects.filter(
                    reduce(operator.or_, [Q(account=account, year=year, month__gt=month),
                                          Q(account=account, year__gt=year)])).order_by('year', 'month')
                if len(next_entries) > 0:
                    if next_entries[0].year == year:
                        dates = [(year, m) for m in range(month + 1, next_entries[0].month)]
                    else:
                        dates = [(year, m) for m in range(month + 1, 13)]
                        for y in range(year + 1, next_entries[0].year):
                            dates += [(y, m) for m in range(1, 13)]
                        dates += [(next_entries[0].year, m) for m in range(1, next_entries[0].month)]
                    for (y, m) in dates:
                        summary_type.create_month_summary_cumulative(account,
                                                                     y,
                                                                     m,
                                                                     account.start_balance + amount, 0)
                    for entry in next_entries:
                        entry.balance += amount
                        entry.save()
                month_summary_cumulative.balance += account.start_balance
                month_summary_cumulative.save()

                from django.utils import timezone
                MonthSummaryCumulative.update_summaries(MonthSummaryCumulative, account, timezone.now())

                return True
            return False

    def get_statement_summary_cumulative(summary_type, accounts, year=None, month=None):
        if year is None and month is None:
            return summary_type.objects.filter(reduce(operator.or_,
                                                      [Q(account=a)
                                                       for a in accounts])).order_by('-year', '-month', 'account')
        elif year is not None and month is None:
            return summary_type.objects.filter(reduce(operator.or_,
                                                      [Q(year=year, account=a)
                                                       for a in accounts])).order_by('-year', '-month', 'account')
        elif year is not None and month is not None:
            return summary_type.objects.filter(reduce(operator.or_,
                                                      [Q(year=year, month=month, account=a)
                                                       for a in accounts])).order_by('-year', '-month', 'account')
        else:
            raise Exception("Incorrect parameters")

    def update_summaries(summary_type, account, date):
        entry = summary_type.objects.filter(account=account).order_by('-year', '-month').first()
        if entry:
            if entry.year < date.year:
                start_month = entry.month + 1
                for year in range(entry.year, date.year):
                    for month in range(start_month, 13):
                        summary_type.objects.create(account=account,
                                                    year=year,
                                                    month=month,
                                                    balance=entry.balance,
                                                    excluded_balance=entry.excluded_balance)
                    start_month = 1
                for month in range(1, date.month + 1):
                    summary_type.objects.create(account=account,
                                                year=date.year,
                                                month=month,
                                                balance=entry.balance,
                                                excluded_balance=entry.excluded_balance)
            elif entry.year == date.year:
                for month in range(entry.month + 1, date.month + 1):
                    summary_type.objects.create(account=account,
                                                year=date.year,
                                                month=month,
                                                balance=entry.balance,
                                                excluded_balance=entry.excluded_balance)


class BaseYearSummaryCumulative(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    year = models.IntegerField()
    balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    excluded_balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)

    class Meta:
        abstract = True
        unique_together = (('account', 'year'))

    def __str__(self):
        return str(self.year) + ": " + str(self.balance)

    def create_year_summary_cumulative(summary_type, account, year, balance, excluded_balance):
        return summary_type.objects.create(account=account, year=year, balance=balance, excluded_balance=excluded_balance)

    def update_year_summary(summary_type, account, year, amount, exclude_amount):
        year_summary_cumulative, year_summary_cumulative_created = summary_type.objects.get_or_create(
            account=account, year=year,
            defaults={'account': account, 'year': year, 'balance': amount, 'excluded_balance': exclude_amount},
        )
        if not year_summary_cumulative_created:
            year_summary_cumulative.balance += amount
            year_summary_cumulative.save()
            next_entries = summary_type.objects.filter(
                reduce(operator.or_, [Q(account=account, year__gt=year)])).order_by('year')
            for entry in next_entries:
                entry.balance += amount
                entry.save()
            return False
        else:
            previous_entry = summary_type.objects.filter(account=account, year__lt=year).order_by('-year').first()
            if previous_entry is not None:
                for y in range(previous_entry.year + 1, year):
                    summary_type.create_year_summary_cumulative(account,
                                                                y,
                                                                previous_entry.balance,
                                                                previous_entry.excluded_balance)
                year_summary_cumulative.balance += previous_entry.balance
                year_summary_cumulative.save()
            else:
                next_entries = summary_type.objects.filter(
                    reduce(operator.or_, [Q(account=account, year__gt=year)])).order_by('year')
                if len(next_entries) > 0:
                    for y in range(year + 1, next_entries[0].year):
                        summary_type.create_year_summary_cumulative(
                            account, y, account.start_balance + amount, account.start_balance + amount)
                    for entry in next_entries:
                        entry.balance += amount
                        entry.save()
                year_summary_cumulative.balance += account.start_balance
                year_summary_cumulative.save()

                from django.utils import timezone
                YearSummaryCumulative.update_summaries(YearSummaryCumulative, account, timezone.now())

                return True
            return False

    def get_statement_summary_cumulative(summary_type, accounts, year=None):
        if accounts is None or len(accounts) == 0:
            return []
        if year is None:
            return summary_type.objects.filter(reduce(operator.or_,
                                                      [Q(account=a)
                                                       for a in accounts])).order_by('-year', 'account')
        else:
            return summary_type.objects.filter(reduce(operator.or_,
                                                      [Q(year=year, account=a)
                                                       for a in accounts])).order_by('-year', 'account')

    def update_summaries(summary_type, account, date):
        entry = summary_type.objects.filter(account=account).order_by('-year').first()
        if entry:
            if entry.year < date.year:
                for year in range(entry.year + 1, date.year + 1):
                    summary_type.objects.create(account=account,
                                                year=year,
                                                balance=entry.balance,
                                                excluded_balance=entry.excluded_balance)


class BaseStatementTypeMonthSummary(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    statement_type = models.ForeignKey(StatementType, on_delete=models.CASCADE)
    year = models.IntegerField()
    month = models.IntegerField()
    balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    excluded_balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)

    class Meta:
        abstract = True
        unique_together = (('account', 'statement_type', 'year', 'month'))

    def __str__(self):
        return str(self.year) + "-" + str(self.month) + ":" + self.statement_type.name + ": " + str(self.balance)

    def create_month_summary(summary_type, account, statement_type, year, month, balance, excluded_balance):
        return summary_type.objects.create(account=account,
                                           statement_type=statement_type,
                                           year=year,
                                           month=month,
                                           balance=balance,
                                           excluded_balance=excluded_balance)

    def update_month_summary(summary_type, account, year, month, statement_type, amount, exclude_amount):
        type_month_summary, type_month_summary_created = summary_type.objects.get_or_create(
            account=account, year=year, month=month, statement_type=statement_type,
            defaults={'account': account, 'year': year, 'month': month, 'balance': amount, 'excluded_balance': exclude_amount},
        )
        if not type_month_summary_created:
            type_month_summary.balance += amount
            type_month_summary.excluded_balance += amount
            type_month_summary.save()

    def get_statement_type_summary(summary_type,
                                   accounts,
                                   year=None,
                                   month=None,
                                   parent_types=None,
                                   types=None,
                                   excluded_types=None):
        query = reduce(operator.or_, [Q(account=a) for a in accounts])
        types_filter = []
        # ToDo write test for
        if parent_types:
            types_filter.append(reduce(operator.or_, [Q(statement_type__parent_type=t) for t in parent_types]))
        if types:
            types_filter.append(reduce(operator.or_, [Q(statement_type=t) for t in types]))
        if excluded_types:
            types_filter.append(reduce(operator.or_, [~Q(statement_type=t) for t in excluded_types]))
        if len(types_filter):
            query &= reduce(operator.or_, types_filter)
        if year is None and month is None:
            return summary_type.objects.filter(query).order_by('-year', '-month', 'account', '-balance')
        if year is not None and month is None:
            return summary_type.objects.filter(query & Q(year=year)).order_by('-year', '-month', 'account', '-balance')
        elif year is not None and month is not None:
            return summary_type.objects.filter(query & Q(year=year, month=month)).order_by('-year',
                                                                                           '-month',
                                                                                           'account',
                                                                                           '-balance')
        else:
            raise Exception("Incorrect parameters")

    def get_statements_expenses(summary_type, accounts, year=None, month=None):
        if accounts is None or len(accounts) == 0:
            return []
        else:
            query = reduce(operator.or_, [Q(account=a) for a in accounts])
            if year:
                query &= Q(year=year)
            if month:
                query &= Q(month=month)
            query &= Q(statement_type__parent_type=20000)
            return summary_type.objects.filter(query).order_by('-year', 'account', 'balance')


class BaseStatementTypeYearSummary(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    statement_type = models.ForeignKey(StatementType, on_delete=models.CASCADE)
    year = models.IntegerField()
    balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)
    excluded_balance = models.DecimalField(max_digits=19, decimal_places=2, default=0)

    class Meta:
        abstract = True
        unique_together = (('account', 'statement_type', 'year'))

    def __str__(self):
        return str(self.year) + ":" + self.statement_type.name + ": " + str(self.balance)

    def create_year_summary(summary_type, account, type, year, balance, excluded_balance):
        return summary_type.objects.create(account=account,
                                           statement_type=type,
                                           year=year,
                                           balance=balance,
                                           excluded_balance=excluded_balance)

    def update_year_summary(summary_type, account, year, statement_type, amount, exclude_amount):
        type_year_summary, type_year_summary_created = summary_type.objects.get_or_create(
            account=account, year=year, statement_type=statement_type,
            defaults={'account': account, 'year': year, 'balance': amount, 'excluded_balance': exclude_amount},
        )
        if not type_year_summary_created:
            type_year_summary.balance += amount
            type_year_summary.excluded_balance += exclude_amount
            type_year_summary.save()

    def get_statements_expenses(summary_type, accounts, year=None):
        if accounts is None or len(accounts) == 0:
            return []
        else:
            query = reduce(operator.or_, [Q(account=a) for a in accounts])
            if year:
                query &= Q(year=year)
            query &= Q(statement_type__parent_type=20000)
            return summary_type.objects.filter(query).order_by('-year', 'account', 'balance')

    def get_statement_type_summary(summary_type, accounts, year=None, parent_types=None, types=None, excluded_types=None):
        if accounts is None or len(accounts) == 0:
            return []
        query = reduce(operator.or_, [Q(account=a) for a in accounts])
        if parent_types:
            query &= reduce(operator.or_, [Q(statement_type__parent_type=t) for t in parent_types])
        if types:
            query &= reduce(operator.or_, [Q(statement_type=t) for t in types])
        if excluded_types:
            query &= reduce(operator.and_, [~Q(statement_type=t) for t in excluded_types])
        if year is None:  # ToDO remove???
            return summary_type.objects.filter(query).order_by('-year', 'account', '-balance')
        elif year is not None:
            return summary_type.objects.filter(query & Q(year=year)).order_by('-year', 'account', '-balance')
        else:
            raise Exception("Incorrect parameters")


class MonthSummaryCumulative(BaseMonthSummaryCumulative):
    def create_month_summary_cumulative(account, year, month, balance, excluded_balance):
        return BaseMonthSummaryCumulative.create_month_summary_cumulative(MonthSummaryCumulative,
                                                                          account,
                                                                          year,
                                                                          month,
                                                                          balance,
                                                                          excluded_balance)

    def update_month_summary(account, year, month, amount, exclude_amount):
        return BaseMonthSummaryCumulative.update_month_summary(MonthSummaryCumulative,
                                                               account,
                                                               year,
                                                               month,
                                                               amount,
                                                               exclude_amount)

    def get_statement_summary_cumulative(accounts, year=None, month=None):
        return BaseMonthSummaryCumulative.get_statement_summary_cumulative(MonthSummaryCumulative, accounts, year, month)


class YearSummaryCumulative(BaseYearSummaryCumulative):
    def create_year_summary_cumulative(account, year, balance, excluded_balance):
        return BaseYearSummaryCumulative.create_year_summary_cumulative(YearSummaryCumulative,
                                                                        account,
                                                                        year,
                                                                        balance,
                                                                        excluded_balance)

    def update_year_summary(account, year, amount, exclude_amount):
        return BaseYearSummaryCumulative.update_year_summary(YearSummaryCumulative, account, year, amount, exclude_amount)

    def get_statement_summary_cumulative(accounts, year=None):
        return BaseYearSummaryCumulative.get_statement_summary_cumulative(YearSummaryCumulative, accounts, year)


class StatementTypeMonthSummary(BaseStatementTypeMonthSummary):
    def create_month_summary(account, statement_type, year, month, balance):
        return BaseStatementTypeMonthSummary.create_month_summary(StatementTypeMonthSummary,
                                                                  account,
                                                                  statement_type,
                                                                  year,
                                                                  month,
                                                                  balance)

    def update_month_summary(account, year, month, statement_type, amount, exclude_amount):
        return BaseStatementTypeMonthSummary.update_month_summary(StatementTypeMonthSummary,
                                                                  account,
                                                                  year,
                                                                  month,
                                                                  statement_type,
                                                                  amount,
                                                                  exclude_amount)

    def get_statement_type_summary(accounts, year=None, month=None, parent_types=None, types=None,
                                   excluded_types=None):
        return BaseStatementTypeMonthSummary.get_statement_type_summary(StatementTypeMonthSummary,
                                                                        accounts,
                                                                        year,
                                                                        month,
                                                                        parent_types,
                                                                        types,
                                                                        excluded_types)

    def get_statements_expenses(accounts, year=None):
        return BaseStatementTypeMonthSummary.get_statements_expenses(StatementTypeMonthSummary, accounts, year)


class StatementTypeYearSummary(BaseStatementTypeYearSummary):
    def create_year_summary(account, statement_type, year, balance):
        return BaseStatementTypeYearSummary.create_year_summary(StatementTypeYearSummary,
                                                                account,
                                                                statement_type,
                                                                year,
                                                                balance)

    def update_year_summary(account, year, statement_type, amount, exclude_amount):
        return BaseStatementTypeYearSummary.update_year_summary(StatementTypeYearSummary,
                                                                account,
                                                                year,
                                                                statement_type,
                                                                amount,
                                                                exclude_amount)

    def get_statement_type_summary(accounts,
                                   year=None,
                                   parent_types=None,
                                   types=None,
                                   excluded_types=None):
        return BaseStatementTypeYearSummary.get_statement_type_summary(StatementTypeYearSummary,
                                                                       accounts,
                                                                       year,
                                                                       parent_types,
                                                                       types,
                                                                       excluded_types)

    def get_statements_expenses(accounts, year=None):
        return BaseStatementTypeYearSummary.get_statements_expenses(StatementTypeYearSummary, accounts, year)


class CombinedAccount(models.Model):
    name = models.CharField(max_length=200, help_text="Name of combinations")
    include_accounts_name = models.CharField(max_length=200, help_text="Name of combinations", default="")
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False, default=1)

    def __str__(self):
        return str(self.name)

    def get_account_using_item(item):
        return CombineItem.objects.select_related('combinedaccount').get(pk=item.pk).combinedaccount

    def add_combined_item(self, account):
        return CombineItem.create_combined_item(self, account)

    def create_account(name, owner, ):
        return CombinedAccount.objects.create(name=name, owner=owner)

    def get_accounts(user):
        return CombinedAccount.objects.filter(owner=user).order_by('name')

    def get_account_by_id(id):
        return CombinedAccount.objects.get(pk=id)

    def create_summaries(self,
                         items=None,
                         create_year_cum=True,
                         create_month_cum=True,
                         create_year_type=True,
                         create_month_type=True):
        if items is None:
            items = CombineItem.objects.filter(combinedaccount=self.pk)

        accounts = reduce(operator.or_, [Q(account__pk=item.account.id) for item in items])

        if create_month_cum:
            CombinedMonthSummaryCumulative.objects.filter(account__pk=self.pk).delete()

            counter = {item.account.pk: 0 for item in items}
            counter_exclude = {item.account.pk: 0 for item in items}
            year = month = 0
            for s in MonthSummaryCumulative.objects.filter(accounts).order_by('year', 'month'):
                if month and month != s.month:
                    CombinedMonthSummaryCumulative.create_month_summary_cumulative(self,
                                                                                   year,
                                                                                   month,
                                                                                   sum([counter[pk]
                                                                                        for pk in counter]),
                                                                                   sum([counter_exclude[pk]
                                                                                        for pk in counter_exclude]))
                counter[s.account.pk] = s.balance
                counter_exclude[s.account.pk] = s.excluded_balance
                month = s.month
                year = s.year
            CombinedMonthSummaryCumulative.create_month_summary_cumulative(self,
                                                                           year,
                                                                           month,
                                                                           sum([counter[pk]
                                                                                for pk in counter]),
                                                                           sum([counter_exclude[pk]
                                                                                for pk in counter_exclude]))

        if create_year_cum:
            CombinedYearSummaryCumulative.objects.filter(account__pk=self.pk).delete()

            counter = {item.account.pk: 0 for item in items}
            counter_exclude = {item.account.pk: 0 for item in items}
            year = 0
            for s in YearSummaryCumulative.objects.filter(accounts).order_by('year'):
                if year and year != s.year:
                    CombinedYearSummaryCumulative.create_year_summary_cumulative(self,
                                                                                 year,
                                                                                 sum([counter[pk]
                                                                                      for pk in counter]),
                                                                                 sum([counter_exclude[pk]
                                                                                      for pk in counter_exclude]))
                counter[s.account.pk] = s.balance
                counter_exclude[s.account.pk] = s.excluded_balance
                year = s.year
            CombinedYearSummaryCumulative.create_year_summary_cumulative(self,
                                                                         year,
                                                                         sum([counter[pk]
                                                                              for pk in counter]),
                                                                         sum([counter_exclude[pk]
                                                                              for pk in counter_exclude]))

        if create_month_type:
            CombinedStatementTypeMonthSummary.objects.filter(account__pk=self.pk).delete()
            for s in StatementTypeMonthSummary.objects.filter(accounts):
                CombinedStatementTypeMonthSummary.update_month_summary(self,
                                                                       s.year,
                                                                       s.month,
                                                                       s.statement_type,
                                                                       s.balance,
                                                                       s.excluded_balance)

        if create_year_type:
            CombinedStatementTypeYearSummary.objects.filter(account__pk=self.pk).delete()
            for s in StatementTypeYearSummary.objects.filter(accounts):
                CombinedStatementTypeYearSummary.update_year_summary(self,
                                                                     s.year,
                                                                     s.statement_type,
                                                                     s.balance,
                                                                     s.excluded_balance)

    def create_sub_name(self):
        items = CombineItem.objects.filter(combinedaccount=self.pk)
        self.include_accounts_name = ", ".join([item.account.name for item in items])
        self.save()


class CombineItem(models.Model):
    combinedaccount = models.ForeignKey(CombinedAccount, null=False, blank=False, default=-1, on_delete=models.CASCADE,)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, null=False, blank=False, default=-1)

    class Meta:
        unique_together = (('combinedaccount', 'account'))

    def get_items(account_id):
        return CombinedAccount.objects.get(pk=account_id).combineitem_set.all()

    def get_combined_account(self):
        return CombinedAccount.objects.get(pk=self.pk)

    def get_accounts(user):
        return Account.objects.all()

    def update_type_summaries(account, year, month, amount, exclude_amount, statement_type):
        for item in CombineItem.objects.filter(account=account):
            CombinedStatementTypeMonthSummary.update_month_summary(item.combinedaccount,
                                                                   year,
                                                                   month,
                                                                   statement_type,
                                                                   amount,
                                                                   exclude_amount)
            CombinedStatementTypeYearSummary.update_year_summary(item.combinedaccount,
                                                                 year,
                                                                 statement_type,
                                                                 amount,
                                                                 exclude_amount)

    def update_cum_summaries(account, year, month, amount, excluded_amount, recreate_year=False, recreate_month=False):
        pass

    def create_combined_item(combinedaccount, account):
        return CombineItem.objects.create(combinedaccount=combinedaccount, account=account)


class CombinedMonthSummaryCumulative(BaseMonthSummaryCumulative):
    account = models.ForeignKey(CombinedAccount, on_delete=models.CASCADE)

    def create_month_summary_cumulative(combined_account, year, month, balance, excluded_balance):
        return BaseMonthSummaryCumulative.create_month_summary_cumulative(CombinedMonthSummaryCumulative,
                                                                          combined_account,
                                                                          year,
                                                                          month,
                                                                          balance,
                                                                          excluded_balance)

    def update_month_summary(combined_account, year, month, amount, excluded_amount):
        return True

    def get_statement_summary_cumulative(combined_account, year=None, month=None):
        return BaseMonthSummaryCumulative.get_statement_summary_cumulative(CombinedMonthSummaryCumulative,
                                                                           combined_account,
                                                                           year,
                                                                           month)


class CombinedYearSummaryCumulative(BaseYearSummaryCumulative):
    account = models.ForeignKey(CombinedAccount, on_delete=models.CASCADE)

    def create_year_summary_cumulative(combined_account, year, balance, excluded_balance):
        return BaseYearSummaryCumulative.create_year_summary_cumulative(CombinedYearSummaryCumulative,
                                                                        combined_account,
                                                                        year,
                                                                        balance,
                                                                        excluded_balance)

    def update_year_summary(combined_account, year, amount, excluded_amount):
        return True

    def get_statement_summary_cumulative(combined_account, year=None):
        return BaseYearSummaryCumulative.get_statement_summary_cumulative(CombinedYearSummaryCumulative,
                                                                          combined_account,
                                                                          year)


class CombinedStatementTypeMonthSummary(BaseStatementTypeMonthSummary):
    account = models.ForeignKey(CombinedAccount, on_delete=models.CASCADE)

    def create_month_summary(account, statement_type, year, month, balance, excluded_balance):
        return BaseStatementTypeMonthSummary.create_month_summary(CombinedStatementTypeMonthSummary,
                                                                  account,
                                                                  statement_type,
                                                                  year,
                                                                  month,
                                                                  balance,
                                                                  excluded_balance)

    def update_month_summary(account, year, month, statement_type, amount, exclude_amount):
        return BaseStatementTypeMonthSummary.update_month_summary(CombinedStatementTypeMonthSummary,
                                                                  account,
                                                                  year,
                                                                  month,
                                                                  statement_type,
                                                                  amount,
                                                                  exclude_amount)

    def get_statement_type_summary(accounts, year=None, month=None, parent_types=None, types=None,
                                   excluded_types=None):
        return BaseStatementTypeMonthSummary.get_statement_type_summary(CombinedStatementTypeMonthSummary,
                                                                        accounts,
                                                                        year,
                                                                        month,
                                                                        parent_types,
                                                                        types,
                                                                        excluded_types)

    def get_statements_expenses(accounts, year=None, month=None):
        return BaseStatementTypeMonthSummary.get_statements_expenses(CombinedStatementTypeMonthSummary,
                                                                     accounts,
                                                                     year,
                                                                     month)


class CombinedStatementTypeYearSummary(BaseStatementTypeYearSummary):
    account = models.ForeignKey(CombinedAccount, on_delete=models.CASCADE)

    def create_year_summary(account, statement_type, year, balance, excluded_balance):
        return BaseStatementTypeYearSummary.create_year_summary(CombinedStatementTypeYearSummary,
                                                                account,
                                                                statement_type,
                                                                year,
                                                                balance,
                                                                excluded_balance)

    def update_year_summary(account, year, statement_type, amount, exclude_amount):
        return BaseStatementTypeYearSummary.update_year_summary(CombinedStatementTypeYearSummary,
                                                                account,
                                                                year,
                                                                statement_type,
                                                                amount,
                                                                exclude_amount)

    def get_statement_type_summary(accounts, year=None, parent_types=None, types=None,
                                   excluded_types=None):
        return BaseStatementTypeYearSummary.get_statement_type_summary(CombinedStatementTypeYearSummary,
                                                                       accounts,
                                                                       year,
                                                                       parent_types,
                                                                       types,
                                                                       excluded_types)

    def get_statements_expenses(accounts, year=None):
        return BaseStatementTypeYearSummary.get_statements_expenses(CombinedStatementTypeYearSummary, accounts, year)


@receiver(post_save, sender=StatementTypeMonthSummary)
def post_month_type_save(sender, instance, **kwargs):
    if instance.balance == 0 and instance.balance == 0:
        instance.delete()


@receiver(pre_save, sender=Statement)
def add_summaries(sender, instance, **kwargs):
    if not instance._state.adding:
        statement = Statement.objects.get(pk=instance.pk)
        if statement.type.pk != instance.type.pk:
            statement_type = statement.type
            while statement_type is not None:
                StatementTypeMonthSummary.update_month_summary(account=statement.account,
                                                               year=statement.date.year,
                                                               month=statement.date.month,
                                                               statement_type=statement_type,
                                                               amount=-statement.amount,
                                                               exclude_amount=0)
                StatementTypeYearSummary.update_year_summary(account=statement.account,
                                                             year=statement.date.year,
                                                             statement_type=statement_type,
                                                             amount=-statement.amount,
                                                             exclude_amount=0)
                statement_type = statement_type.parent_type
            statement_type = instance.type
            while statement_type is not None:
                StatementTypeMonthSummary.update_month_summary(account=statement.account,
                                                               year=statement.date.year,
                                                               month=statement.date.month,
                                                               statement_type=statement_type,
                                                               amount=statement.amount,
                                                               exclude_amount=0)
                StatementTypeYearSummary.update_year_summary(account=statement.account,
                                                             year=statement.date.year,
                                                             statement_type=statement_type,
                                                             amount=statement.amount,
                                                             exclude_amount=0)
                statement_type = statement_type.parent_type
    else:
        Statement.update_summaries(instance.account, instance.date, instance.amount, instance.type)


@receiver(post_delete, sender=Statement)
def subtract_summaries(sender, instance, **kwargs):
    Statement.update_summaries(instance.account, instance.date, -instance.amount, instance.type)


@receiver(post_delete, sender=Account)
def account_removed_fix_summaries(sender, instance, **kwargs):
    for summary in MonthSummaryCumulative.objects.filter(account=instance):
        summary.delete()
    for summary in YearSummaryCumulative.objects.filter(account=instance):
        summary.delete()
    for summary in StatementTypeMonthSummary.objects.filter(account=instance):
        summary.delete()
    for summary in StatementTypeYearSummary.objects.filter(account=instance):
        summary.delete()
    for item in CombineItem.objects.filter(account=instance):
        item.delete()


@receiver(pre_save, sender=MonthSummaryCumulative)
def pre_month_cum_save(sender, instance, **kwargs):
    items = CombineItem.objects.filter(account=instance.account)
    if instance.id is None:
        for item in items:
            entry = CombinedMonthSummaryCumulative.objects.filter(account=item.combinedaccount,
                                                                  year=instance.year,
                                                                  month=instance.month)
            if len(entry) == 0:
                CombinedMonthSummaryCumulative.create_month_summary_cumulative(item.combinedaccount,
                                                                               instance.year,
                                                                               instance.month,
                                                                               instance.balance,
                                                                               instance.excluded_balance)
            else:
                entry[0].balance += instance.balance
                entry[0].excluded_balance += instance.excluded_balance
                entry[0].save()
    else:
        previous_entry = MonthSummaryCumulative.objects.get(pk=instance.id)
        for item in items:
            entry = CombinedMonthSummaryCumulative.objects.filter(account=item.combinedaccount,
                                                                  year=instance.year,
                                                                  month=instance.month)
            if len(entry) == 0:
                raise Exception("Unhandled case")
            else:
                entry[0].balance += instance.balance - previous_entry.balance
                entry[0].excluded_balance += instance.excluded_balance - previous_entry.excluded_balance
                entry[0].save()


@receiver(pre_save, sender=YearSummaryCumulative)
def pre_year_cum_save(sender, instance, **kwargs):
    items = CombineItem.objects.filter(account=instance.account)
    if instance.id is None:
        for item in items:
            entry = CombinedYearSummaryCumulative.objects.filter(account=item.combinedaccount, year=instance.year)
            if len(entry) == 0:
                CombinedYearSummaryCumulative.create_year_summary_cumulative(item.combinedaccount,
                                                                             instance.year,
                                                                             instance.balance,
                                                                             instance.excluded_balance)
            else:
                entry[0].balance += instance.balance
                entry[0].excluded_balance += instance.excluded_balance
                entry[0].save()
    else:
        previous_entry = YearSummaryCumulative.objects.get(pk=instance.id)
        for item in items:
            entry = CombinedYearSummaryCumulative.objects.filter(account=item.combinedaccount, year=instance.year)
            if len(entry) == 0:
                raise Exception("Unhandled case")
            else:
                entry[0].balance += instance.balance - previous_entry.balance
                entry[0].excluded_balance += instance.excluded_balance - previous_entry.excluded_balance
                entry[0].save()


@receiver(pre_save, sender=StatementTypeMonthSummary)
def pre_month_type_save(sender, instance, **kwargs):
    items = CombineItem.objects.filter(account=instance.account)
    if instance.id is None:
        for item in items:
            entry = CombinedStatementTypeMonthSummary.objects.filter(account=item.combinedaccount,
                                                                     statement_type=instance.statement_type,
                                                                     year=instance.year,
                                                                     month=instance.month)
            if len(entry) == 0:
                CombinedStatementTypeMonthSummary.create_month_summary(item.combinedaccount,
                                                                       instance.statement_type,
                                                                       instance.year,
                                                                       instance.month,
                                                                       instance.balance,
                                                                       instance.excluded_balance)
            else:
                entry[0].balance += instance.balance
                entry[0].excluded_balance += instance.excluded_balance
                entry[0].save()
    else:
        previous_entry = StatementTypeMonthSummary.objects.get(pk=instance.id)
        for item in items:
            entry = CombinedStatementTypeMonthSummary.objects.filter(account=item.combinedaccount,
                                                                     statement_type=instance.statement_type,
                                                                     year=instance.year,
                                                                     month=instance.month)
            if len(entry) == 0:
                raise Exception("Unhandled case")
            else:
                entry[0].balance += instance.balance - previous_entry.balance
                entry[0].excluded_balance += instance.excluded_balance - previous_entry.excluded_balance
                entry[0].save()


@receiver(pre_save, sender=StatementTypeYearSummary)
def pre_year_type_save(sender, instance, **kwargs):
    items = CombineItem.objects.filter(account=instance.account)
    if instance.id is None:
        for item in items:
            entry = CombinedStatementTypeYearSummary.objects.filter(account=item.combinedaccount,
                                                                    statement_type=instance.statement_type,
                                                                    year=instance.year)
            if len(entry) == 0:
                CombinedStatementTypeYearSummary.create_year_summary(item.combinedaccount,
                                                                     instance.statement_type,
                                                                     instance.year,
                                                                     instance.balance,
                                                                     instance.excluded_balance)
            else:
                entry[0].balance += instance.balance
                entry[0].excluded_balance += instance.excluded_balance
                entry[0].save()
    else:
        previous_entry = StatementTypeYearSummary.objects.get(pk=instance.id)
        for item in items:
            entry = CombinedStatementTypeYearSummary.objects.filter(account=item.combinedaccount, statement_type=instance.statement_type, year=instance.year)  # noqa
            if len(entry) == 0:
                raise Exception("Unhandled case")
            else:
                entry[0].balance += instance.balance - previous_entry.balance
                entry[0].excluded_balance += instance.excluded_balance - previous_entry.excluded_balance
                entry[0].save()


@receiver(post_save, sender=StatementTypeYearSummary)
def post_year_type_save(sender, instance, **kwargs):
    if instance.balance == 0 and instance.balance == 0:
        instance.delete()


@receiver(post_save, sender=CombineItem)
def combine_item_added(sender, instance, **kwargs):
    combine_month_summary = list(CombinedMonthSummaryCumulative.objects.filter(account=instance.combinedaccount).order_by('year', 'month'))  # noqa
    summary_entries = list(MonthSummaryCumulative.objects.filter(account=instance.account).order_by('year', 'month'))
    if len(combine_month_summary) == 0:
        for s in summary_entries:
            CombinedMonthSummaryCumulative.create_month_summary_cumulative(instance.combinedaccount,
                                                                           s.year,
                                                                           s.month,
                                                                           s.balance,
                                                                           s.excluded_balance)
    else:
        start_date = (combine_month_summary[0].year, combine_month_summary[0].month)
        end_date = (combine_month_summary[-1].year, combine_month_summary[-1].month)
        add_balance = combine_month_summary[-1].balance
        add_excluded_balance = combine_month_summary[-1].excluded_balance
        if summary_entries[0].year < start_date[0] or \
           (summary_entries[0].year == start_date[0] and summary_entries[0].month < start_date[1]):
            start_date = (summary_entries[0].year, summary_entries[0].month)
        if summary_entries[-1].year > end_date[0] or \
           (summary_entries[-1].year == end_date[0] and summary_entries[-1].month > end_date[1]):
            end_date = (summary_entries[-1].year, summary_entries[-1].month)
        else:
            add_balance = summary_entries[-1].balance
            add_excluded_balance = summary_entries[-1].balance
        start_month = start_date[1]
        combined_iter = iter(combine_month_summary)
        summary_iter = iter(summary_entries)
        combined = next(combined_iter, None)
        summary = next(summary_iter, None)

        def match_date(entry, year, month):
            return entry.year == year and entry.month == month

        counter = 0
        for year in range(start_date[0], end_date[0] + 1):
            for month in range(start_month, 13 if year < end_date[0] else end_date[1] + 1):
                if combined is None and summary is not None:
                    if match_date(summary, year, month):
                        CombinedMonthSummaryCumulative.create_month_summary_cumulative(instance.combinedaccount,
                                                                                       year,
                                                                                       month,
                                                                                       summary.balance + add_balance,
                                                                                       summary.excluded_balance +
                                                                                       add_excluded_balance)
                        summary = next(summary_iter, None)
                        counter += 1
                    else:
                        CombinedMonthSummaryCumulative.create_month_summary_cumulative(instance.combinedaccount,
                                                                                       year,
                                                                                       month,
                                                                                       add_balance,
                                                                                       add_excluded_balance)
                        counter += 1
                elif summary is None and combined is not None:
                    if match_date(combined, year, month):
                        combined.balance += add_balance
                        combined.excluded_balance += add_excluded_balance
                        combined.save()
                        counter += 1
                    else:
                        CombinedMonthSummaryCumulative.create_month_summary_cumulative(instance.combinedaccount,
                                                                                       year,
                                                                                       month,
                                                                                       add_balance,
                                                                                       add_excluded_balance)
                        counter += 1
                else:
                    if match_date(combined, year, month) and match_date(summary, year, month):
                        combined.balance += summary.balance
                        combined.excluded_balance += summary.excluded_balance
                        combined.save()
                        combined = next(combined_iter, None)
                        summary = next(summary_iter, None)
                        counter += 1
                    elif match_date(combined, year, month):
                        combined = next(combined_iter, None)
                        counter += 1
                    elif match_date(summary, year, month):
                        CombinedMonthSummaryCumulative.create_month_summary_cumulative(instance.combinedaccount,
                                                                                       year,
                                                                                       month,
                                                                                       summary.balance,
                                                                                       summary.excluded_balance)
                        summary = next(summary_iter, None)
                        counter += 1
                    else:
                        raise Exception("Unhandled case")
            start_month = 1

    combine_year_summary = list(CombinedYearSummaryCumulative.objects.filter(account=instance.combinedaccount).order_by('year'))
    summary_entries = list(YearSummaryCumulative.objects.filter(account=instance.account).order_by('year'))
    if len(combine_year_summary) == 0:
        for s in summary_entries:
            CombinedYearSummaryCumulative.create_year_summary_cumulative(instance.combinedaccount,
                                                                         s.year,
                                                                         s.balance,
                                                                         s.excluded_balance)
    else:
        start_year = combine_year_summary[0].year
        end_year = combine_year_summary[-1].year
        add_balance = combine_year_summary[-1].balance
        add_excluded_balance = combine_year_summary[-1].excluded_balance
        if summary_entries[0].year < start_year:
            start_year = summary_entries[0].year
        if summary_entries[-1].year > end_year:
            end_year = summary_entries[-1].year
        else:
            add_balance = summary_entries[-1].balance
            add_excluded_balance = summary_entries[-1].balance
        combined_iter = iter(combine_year_summary)
        summary_iter = iter(summary_entries)
        combined = next(combined_iter, None)
        summary = next(summary_iter, None)
        counter = 0
        for year in range(start_year, end_year + 1):
            if combined is None and summary is not None:
                if summary.year == year:
                    CombinedYearSummaryCumulative.create_year_summary_cumulative(instance.combinedaccount,
                                                                                 year,
                                                                                 summary.balance + add_balance,
                                                                                 summary.excluded_balance + add_excluded_balance)
                    summary = next(summary_iter, None)
                    counter += 1
                else:
                    CombinedYearSummaryCumulative.create_year_summary_cumulative(instance.combinedaccount,
                                                                                 year,
                                                                                 add_balance,
                                                                                 add_excluded_balance)
                    counter += 1
            elif summary is None and combined is not None:
                if combined.year == year:
                    combined.balance += add_balance
                    combined.excluded_balance += add_excluded_balance
                    combined.save()
                    counter += 1
                else:
                    CombinedYearSummaryCumulative.create_year_summary_cumulative(instance.combinedaccount,
                                                                                 year,
                                                                                 add_balance,
                                                                                 add_excluded_balance)
                    counter += 1
            else:
                if combined.year == year and summary.year == year:
                    combined.balance += summary.balance
                    combined.excluded_balance += summary.excluded_balance
                    combined.save()
                    combined = next(combined_iter, None)
                    summary = next(summary_iter, None)
                    counter += 1
                elif combined.year == year:
                    combined = next(combined_iter, None)
                    counter += 1
                elif summary.year == year:
                    CombinedYearSummaryCumulative.create_year_summary_cumulative(instance.combinedaccount,
                                                                                 year,
                                                                                 summary.balance,
                                                                                 summary.excluded_balance)
                    summary = next(summary_iter, None)
                    counter += 1
                else:
                    raise Exception("Unhandled case")

    summary_entries = list(StatementTypeMonthSummary.objects.filter(account=instance.account).order_by('year', 'month'))
    for summary in summary_entries:
        type_summary, created = CombinedStatementTypeMonthSummary.objects.get_or_create(
            account=instance.combinedaccount, year=summary.year, month=summary.month, statement_type=summary.statement_type,
            defaults={'account': instance.combinedaccount,
                      'year': summary.year,
                      'month': summary.month,
                      'balance': summary.balance,
                      'excluded_balance': summary.excluded_balance},)
        if not created:
            type_summary.balance += summary.balance
            type_summary.excluded_balance += summary.excluded_balance
            type_summary.save()
    summary_entries = list(StatementTypeYearSummary.objects.filter(account=instance.account).order_by('year'))
    for summary in summary_entries:
        type_summary, created = CombinedStatementTypeYearSummary.objects.get_or_create(
            account=instance.combinedaccount, year=summary.year, statement_type=summary.statement_type,
            defaults={'account': instance.combinedaccount,
                      'year': summary.year,
                      'balance': summary.balance,
                      'excluded_balance': summary.excluded_balance},)
        if not created:
            type_summary.balance += summary.balance
            type_summary.excluded_balance += summary.excluded_balance
            type_summary.save()


@receiver(post_save, sender=CombinedStatementTypeMonthSummary)
def post_combined_month_type_save(sender, instance, **kwargs):
    if instance.balance == 0 and instance.balance == 0:
        instance.delete()


@receiver(post_save, sender=CombinedStatementTypeYearSummary)
def post_combined_year_type_save(sender, instance, **kwargs):
    if instance.balance == 0 and instance.balance == 0:
        instance.delete()
