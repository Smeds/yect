from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError, transaction
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import redirect
from django.urls import reverse
from .forms import AccountForm
from .forms import CombinedAccountForm
from .forms import CombinedItemForm
from .forms import StatementForm
from .decorators import validate_account_access
from .decorators import validate_statement_access
from .models import Account
from .models import CombinedAccount
from .models import CombineItem
from .models import Statement
from .models import StatementType
from .models import YearSummaryCumulative
from .models import MonthSummaryCumulative
from .models import StatementTypeYearSummary
from .models import StatementTypeMonthSummary
from .models import CombinedYearSummaryCumulative
from .models import CombinedMonthSummaryCumulative
from .models import CombinedStatementTypeYearSummary
from .models import CombinedStatementTypeMonthSummary
from .utils.dataStructurGenerators import add_missing_month_entries
from .utils.dataStructurGenerators import discrete_bar_plot
from .utils.dataStructurGenerators import discrete_bar_plot_mean
from .utils.dataStructurGenerators import stacked_area_chart
from .utils.dataStructurGenerators import create_date_range_and_populate
from .utils.dataStructurGenerators import piechart
from .utils.dataStructurGenerators import decimal_default
from .utils.data import process_records
from collections import OrderedDict
from statistics import mean
from statistics import stdev
import datetime
import time


# ToDo Change to more appropriate name
def add_account_for_menu(request):
    if request.user.is_anonymous:
        return {'accounts': list(), 'accounts_combined': list(), "current_year": datetime.datetime.now().year}
    else:
        return {'accounts': Account.objects.filter(owner=request.user.pk),
                'accounts_combined': CombinedAccount.objects.filter(owner=request.user.pk),
                "current_year": datetime.datetime.now().year}


# ToDo only for development
def index(request):
    context = {'statements': {'list': Statement.objects.all()}}
    template = loader.get_template("bankbook/statements.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def edit_account(request, account_type=None, account_id=None):
    context = add_account_for_menu(request)
    context["type"] = "a"
    if request.method == 'POST':
        if "save_account" in request.POST:
            if account_id and account_type == "a":
                context['account_form'] = AccountForm(request.POST, instance=Account.objects.get(id=account_id))
            else:
                context['account_form'] = AccountForm(request.POST)
            if context['account_form'].is_valid():
                new_account = context['account_form'].save()
                return redirect(reverse('bankbook:account_summary', kwargs={"account_type": 'a', "account_id": new_account.id}))
        elif "save_combinedaccount" in request.POST:
            if account_id and account_type == "c":
                combined_account = CombinedAccount.objects.get(id=account_id)
                context['combined_account_form'] = CombinedAccountForm(request.POST, instance=combined_account)
                context['combined_item_formset'] = CombinedItemForm.combined_item_formset(request.POST, initial=[{'account': item.account} for item in CombineItem.objects.filter(combinedaccount=combined_account)])  # noqa
            else:
                context['combined_account_form'] = CombinedAccountForm(request.POST)
                context['combined_item_formset'] = CombinedItemForm.create_combine_item_form_set()(request.POST)
            if context['combined_account_form'].is_valid() and context['combined_item_formset'].is_valid():
                try:
                    with transaction.atomic():
                        combined_account_saved = context['combined_account_form'].save()
                        if account_id:
                            CombineItem.objects.filter(combinedaccount=combined_account_saved).delete()
                        accounts_added = [CombineItem(combinedaccount=combined_account_saved,
                                                      account=item.cleaned_data.get('account'))
                                          for item in context['combined_item_formset']]
                        CombineItem.objects.bulk_create(accounts_added)

                        combined_account_saved.include_accounts_name = ", ".join([item.account.name for item in accounts_added])
                        combined_account_saved.save()
                        combined_account_saved.create_summaries()
                        # ToDO setup summary...
                except IntegrityError:
                    # ToDo handle better
                    raise Exception()
                return redirect(reverse('bankbook:account_summary',
                                        kwargs={"account_type": 'c', "account_id": combined_account_saved.id}))
    else:
        if account_type is not None and account_id is not None:
            context["account_id"] = account_id
            context["type"] = account_type
            if account_type == "a":
                context['account_form'] = AccountForm(instance=Account.objects.get(pk=account_id))
            else:
                combined_account = CombinedAccount.objects.get(id=account_id)
                context['account_name'] = combined_account.name
                context['combined_account_form'] = CombinedAccountForm(instance=combined_account)
                context['combined_item_formset'] = CombinedItemForm.create_combine_item_form_set()(
                    form_kwargs={'user': request.user},
                    initial=[
                             {'account': item.account} for item in CombineItem.objects.filter(combinedaccount=combined_account)])
        else:
            context['account_form'] = AccountForm(owner=request.user)
            context['combined_account_form'] = CombinedAccountForm(owner=request.user)
            context['combined_item_formset'] = CombinedItemForm.create_combine_item_form_set(1)(form_kwargs={'owner': request.user})  # noqa
    template = loader.get_template("bankbook/edit_account.html")
    return HttpResponse(template.render(context, request))


def generate_type_statistics(type_summary, key_generator):
    def initiate_data(x):
        return {'income': {'balance': 0, 'excluded_balance': 0},
                'saving': {'balance': 0, 'excluded_balance': 0},
                'transaction': {'balance': 0, 'excluded_balance': 0},
                'expense': {'balance': 0, 'excluded_balance': 0},
                'unknown': {'balance': 0, 'excluded_balance': 0},
                'expense_type': [],
                'year': x.year,
                'month': x.month}
    data = {}
    data_total = {'income': {'balance': [], 'excluded_balance': []},
                  'saving': {'balance': [], 'excluded_balance': []},
                  'transaction': {'balance': [], 'excluded_balance': []},
                  'expense': {'balance': [], 'excluded_balance': []},
                  'unknown': {'balance': [], 'excluded_balance': []},
                  'balance': {'balance': 0, 'excluded_balance': 0}}
    date_range_min = (1000000, 1000000)
    date_range_max = (0, 0)
    for t in type_summary:
        if t.statement_type.pk == 10000:
            data_total['income']['balance'].append(decimal_default(t.balance))
            data_total['income']['excluded_balance'].append(decimal_default(t.excluded_balance))
            try:
                data[key_generator(t)]['income']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['income']['decimal_default(excluded_balance)'] += decimal_default(t.excluded_balance)
            except KeyError:
                data[key_generator(t)] = initiate_data(t)
                data[key_generator(t)]['income']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['income']['excluded_balance'] += decimal_default(t.excluded_balance)
        elif t.statement_type.pk == 15000:
            data_total['saving']['balance'].append(decimal_default(t.balance))
            data_total['saving']['excluded_balance'].append(decimal_default(t.excluded_balance))
            try:
                data[key_generator(t)]['saving']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['saving']['excluded_balance'] += decimal_default(t.excluded_balance)
            except KeyError:
                data[key_generator(t)] = initiate_data(t)
                data[key_generator(t)]['saving']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['saving']['excluded_balance'] += decimal_default(t.excluded_balance)
        elif t.statement_type.pk == 16000:
            data_total['transaction']['balance'].append(decimal_default(t.balance))
            data_total['transaction']['excluded_balance'].append(decimal_default(t.excluded_balance))
            try:
                data[key_generator(t)]['transaction']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['transaction']['excluded_balance'] += decimal_default(t.excluded_balance)
            except KeyError:
                data[key_generator(t)] = initiate_data(t)
                data[key_generator(t)]['transaction']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['transaction']['excluded_balance'] += decimal_default(t.excluded_balance)
        elif t.statement_type.pk == 20000:
            data_total['expense']['balance'].append(decimal_default(t.balance))
            data_total['expense']['excluded_balance'].append(decimal_default(t.excluded_balance))
            try:
                data[key_generator(t)]['expense']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['expense']['excluded_balance'] += decimal_default(t.excluded_balance)
            except KeyError:
                data[key_generator(t)] = initiate_data(t)
                data[key_generator(t)]['expense']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['expense']['excluded_balance'] += decimal_default(t.excluded_balance)
        elif t.statement_type.pk == 1:
            data_total['unknown']['balance'].append(decimal_default(t.balance))
            data_total['unknown']['excluded_balance'].append(decimal_default(t.excluded_balance))
            try:
                data[key_generator(t)]['unknown']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['unknown']['excluded_balance'] += decimal_default(t.excluded_balance)
            except KeyError:
                data[key_generator(t)] = initiate_data(t)
                data[key_generator(t)]['unknown']['balance'] += decimal_default(t.balance)
                data[key_generator(t)]['unknown']['excluded_balance'] += decimal_default(t.excluded_balance)
        else:
            try:
                data[key_generator(t)]['expense_type'].append(t)
            except KeyError:
                data[key_generator(t)] = initiate_data(t)
                data[key_generator(t)]['expense_type'].append(t)
        if t.year < date_range_min[0] or (t.year <= date_range_min[0] and t.month <= date_range_min[1]):
            date_range_min = (t.year, t.month)
        if t.year > date_range_max[0] or (t.year >= date_range_max[0] and t.month >= date_range_max[1]):
            date_range_max = (t.year, t.month)
    for d in data:
        data[d]['expense_type'] = discrete_bar_plot(data[d]['expense_type'])
    return data_total, data, date_range_min, date_range_max


@login_required
@validate_account_access
def account_summary(request, account_type, account_id, year=None, month=None, day=None):
    context = add_account_for_menu(request)
    if year is None:
        def key_generator(x):
            return str(x.year) + "-" + str(x.month)
    else:
        def key_generator(x):
            return x.month
    accounts = []
    cum_summary, cum_summary_year, type_summary = None, None, None
    if account_type == 'a':
        accounts = [Account.get_account_by_id(account_id)]
        cum_summary = MonthSummaryCumulative
        cum_summary_year = YearSummaryCumulative
        type_summary = StatementTypeMonthSummary
    elif account_type == 'c':
        accounts = [CombinedAccount.get_account_by_id(account_id)]
        cum_summary = CombinedMonthSummaryCumulative
        cum_summary_year = CombinedYearSummaryCumulative
        type_summary = CombinedStatementTypeMonthSummary
        context['included_accounts'] = accounts[0].include_accounts_name
    summary = cum_summary.get_statement_summary_cumulative(accounts, year=year, month=month)
    type_summary = type_summary.get_statement_type_summary(
        accounts,
        year=year,
        month=month,
        parent_types=StatementType.get_statement_types(['Expenses']),
        types=[10000, 15000, 16000, 20000, 1])
    context['account_id'] = account_id
    context['account_type'] = account_type
    context['account_name'] = accounts[0].name
    context['selected_year'] = year if year is not None else ". . year . ."
    context['year_list'] = [year.year for year in cum_summary_year.get_statement_summary_cumulative(accounts)]
    if account_type == 'a':
        context['summary'] = stacked_area_chart(summary)
    else:
        context['summary'] = stacked_area_chart(MonthSummaryCumulative.get_statement_summary_cumulative([a.account for a in CombineItem.get_items(account_id)], year=year, month=month))  # noqa
    context['month'] = generate_type_statistics(type_summary, key_generator)[1]
    for s in summary:
        key = key_generator(s)
        if key in context['month']:
            context['month'][key]['balance'] = s.balance
        else:
            context['month'][key] = {'balance': s.balance, 'year': s.year, 'month': s.month}
    template = loader.get_template("bankbook/account_summary.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def statements(request, account_type, account_id, year=None, month=None, day=None):
    context = add_account_for_menu(request)
    if year is None:
        def key_generator(x):
            return str(x.year) + "-" + str(x.month)
    else:
        def key_generator(x):
            return x.month
    accounts, accounts_for_statements, cum_summary, type_summary = [], [], None, None
    if account_type == 'a':
        accounts = [Account.get_account_by_id(account_id)]
        accounts_for_statements = [Account.get_account_by_id(account_id)]
        cum_summary = MonthSummaryCumulative
        type_summary = StatementTypeMonthSummary
    elif account_type == 'c':
        accounts = [CombinedAccount.get_account_by_id(account_id)]
        accounts_for_statements = [item.account for item in CombineItem.get_items(account_id)]
        cum_summary = CombinedMonthSummaryCumulative
        type_summary = CombinedStatementTypeMonthSummary
        context['included_accounts'] = accounts[0].include_accounts_name
    summary = cum_summary.get_statement_summary_cumulative(accounts, year=year, month=month)
    type_summary = type_summary.get_statement_type_summary(
        accounts,
        year=year,
        month=month,
        parent_types=StatementType.get_statement_types(['Expenses']),
        types=[10000, 15000, 16000, 20000, 1])
    context['account_id'] = account_id
    context['account_type'] = account_type
    context['account_name'] = accounts[0].name
    if day is not None:
        context['summary'] = stacked_area_chart(summary)
    context['month'] = generate_type_statistics(type_summary, key_generator)[1]
    print(context['month'])
    for s in summary:
        context['month'][key_generator(s)]['balance'] = s.balance
    statement_list = Statement.get_statements(accounts=accounts_for_statements, year=year, month=month, day=day)
    if month is not None:
        context['month'][int(month)]['statements'] = statement_list
    else:
        for statement in statement_list:
            try:
                context['month'][key_generator(statement.date)]['statements'].append(statement)
            except KeyError:
                context['month'][key_generator(statement.date)]['statements'] = [statement]
    template = loader.get_template("bankbook/statements.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def reports(request, account_type, account_id, year=None):
    if year is None:
        year = datetime.datetime.now().year
    context = add_account_for_menu(request)

    def key_generator(x):
        return x.month

    if account_type == 'a':
        accounts = [Account.get_account_by_id(account_id)]
        cum_summary = MonthSummaryCumulative
        cum_summary_year = YearSummaryCumulative
        type_summary = StatementTypeMonthSummary
        type_summary_year = StatementTypeYearSummary
    elif account_type == 'c':
        accounts = [CombinedAccount.get_account_by_id(account_id)]
        cum_summary = CombinedMonthSummaryCumulative
        cum_summary_year = CombinedYearSummaryCumulative
        type_summary = CombinedStatementTypeMonthSummary
        type_summary_year = CombinedStatementTypeYearSummary
        context['included_accounts'] = accounts[0].include_accounts_name
    context['account_id'] = account_id
    context['timestamp_id'] = str(int(time.time())) + "_" + str(account_id)
    context['account_type'] = account_type
    context['account_name'] = accounts[0].name
    context['selected_year'] = year if year is not None else ". . year . ."
    type_summary_data_total, type_summary_data, min_date, max_date = \
        generate_type_statistics(type_summary.get_statement_type_summary(
                                                                         accounts=accounts,
                                                                         year=year,
                                                                         types=[10000, 15000, 16000, 20000, 1]), key_generator)
    add_missing_month_entries(type_summary_data_total, min_date[1], max_date[1])
    context['year_summary'] = {entry: {"balance": sum(type_summary_data_total[entry]['balance']), 'excluded_balance': sum(type_summary_data_total[entry]['excluded_balance'])} for entry in ["income", 'saving', 'transaction', 'expense', 'unknown']}  # noqa
    context['month_avg_summary'] = {entry: {"avg": mean(type_summary_data_total[entry]['balance']) if len(type_summary_data_total[entry]['balance']) > 0 else 0, 'stdev': stdev(type_summary_data_total[entry]['balance']) if len(type_summary_data_total[entry]['balance']) > 2 else "NA"} for entry in ["income", 'saving', 'transaction', 'expense', 'unknown']}  # noqa

    context['year_balance_plot'] = [{"key": "balance", "values": [{'x': entry.year, 'y': decimal_default(entry.balance)} for entry in cum_summary_year.get_statement_summary_cumulative(accounts)]}]  # noqa
    for entry in context['year_balance_plot'][0]['values']:
        if entry['x'] == int(year):
            context['year_summary']['balance'] = {'balance': entry['y']}
    context['month_balance_plot'] = [{"key": "balance",
                                     "values": [{'x': entry.month, 'y': decimal_default(entry.balance)} for entry in
                                                cum_summary.get_statement_summary_cumulative(accounts, year=year)]}]

    context['summary_plot'] = [
        create_date_range_and_populate(data=type_summary_data, label='income', min_month=min_date[1], max_month=max_date[1]),
        create_date_range_and_populate(data=type_summary_data, label='expense', min_month=min_date[1], max_month=max_date[1]),
        create_date_range_and_populate(data=type_summary_data, label='saving', min_month=min_date[1], max_month=max_date[1]),
        create_date_range_and_populate(data=type_summary_data, label='transaction', min_month=min_date[1], max_month=max_date[1])]

    context['month_avr_type_summary'] = discrete_bar_plot_mean(
        type_summary.get_statements_expenses(accounts, year), min_date[1], max_date[1], sort_by_amount=True)
    type_data_year = type_summary_year.get_statements_expenses(accounts, year)
    context['year_type_summary'] = discrete_bar_plot(type_data_year)

    context['piecharts'] = []
    for d in type_data_year:
        pie_data = {'title': d.statement_type.name,
                    'id': d.statement_type.pk,
                    'data': piechart(type_summary_year.get_statement_type_summary(accounts,
                                                                                  year=year,
                                                                                  parent_types=[d.statement_type.pk]))[0]}
        if len(pie_data['data']) > 1:
            context['piecharts'].append(pie_data)
    template = loader.get_template("bankbook/reports.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def cum_summary_month(request, account_type=None, account_id=None, year=None, month=None):
    accounts = Account.objects.all()
    context = {'summary':
               stacked_area_chart(MonthSummaryCumulative.get_statement_summary_cumulative(accounts, year, month))}
    template = loader.get_template("bankbook/cum_summary.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def cum_summary_year(request, account_type=None, account_id=None, year=None):
    # accounts = [Account.get_account_by_id(account_id)]
    accounts = Account.objects.all()
    context = {'summary':
               stacked_area_chart(YearSummaryCumulative.get_statement_summary_cumulative(accounts, year), True)}
    template = loader.get_template("bankbook/cum_summary.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def type_summary_month(request, account_type, account_id, year=None, month=None):
    accounts = [Account.get_account_by_id(account_id)]
    month_summary = OrderedDict()
    for s_item in StatementTypeMonthSummary.get_statement_type_summary(accounts, year, month, parent_types=StatementType.get_statement_types(['Expenses'])):  # noqa
        date_string = "{}-{}".format(s_item.year, s_item.month)
        if date_string in month_summary:
            month_summary[date_string].append(s_item)
        else:
            month_summary[date_string] = [s_item]
    context = {'summary': {k: discrete_bar_plot(month_summary[k]) for k in month_summary}}
    template = loader.get_template("bankbook/type_summary.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def type_summary_year(request, account_type, account_id, year=None):
    accounts = [Account.get_account_by_id(account_id)]
    year_summary = OrderedDict()
    for s_item in StatementTypeYearSummary.get_statement_type_summary(accounts, year, parent_types=StatementType.get_statement_types(['Expenses'])):  # noqa
        if s_item.year in year_summary:
            year_summary[s_item.year].append(s_item)
        else:
            year_summary[s_item.year] = [s_item]
    context = {'summary': {k: discrete_bar_plot(year_summary[k]) for k in year_summary}}
    template = loader.get_template("bankbook/type_summary.html")
    return HttpResponse(template.render(context, request))


@login_required
@validate_statement_access
def statement_edit(request, statement_id):
    template = loader.get_template("bankbook/edit_statement.html")
    statement = Statement.objects.get(id=statement_id)
    context = {'account_name': statement.account.name, 'statement_id': statement_id}
    if request.method == 'POST':
        form = StatementForm(request.POST, instance=statement)
        if form.is_valid():
            form.save()
            return redirect(
                reverse('bankbook:statements', kwargs={"account_type": 'a', "account_id": statement.account.id,
                                                       "year": statement.date.year, "month": statement.date.month}))
        else:
            context['form'] = form
    else:
        context['form'] = StatementForm(instance=statement)
    return HttpResponse(template.render(context, request))


@login_required
@validate_statement_access
def statement_delete(request, statement_id):
    template = loader.get_template("bankbook/delete_statement.html")
    statement = Statement.objects.get(id=statement_id)
    account_id = statement.account.pk
    context = {'account_name': statement.account.name}
    if request.method == 'POST':
        if request.POST.get("confirm", None):
            statement.delete()
            return HttpResponseRedirect(reverse('bankbook:account_summary',
                                                kwargs={'account_type': 'a',
                                                        'account_id': account_id,
                                                        'year': datetime.datetime.now().year}))
        elif request.POST.get("cancel", None):
            return HttpResponseRedirect(reverse('bankbook:statement_edit',
                                                kwargs={'statement_id': statement_id}))
    else:
        context['statement_id'] = statement_id
        context['statement_text'] = statement.text
        context['statement_amount'] = statement.amount
    return HttpResponse(template.render(context, request))


@login_required
@validate_account_access
def upload_statements(request, account_type, account_id):
    context = add_account_for_menu(request)
    #  ToDo need to change so that only can upload to viewed account
    # account = upload_statements(request)
    context['account_id'] = account_id
    template = loader.get_template("bankbook/upload_statements.html")
    if request.method == 'POST':
        context['form'] = UploadFileForm(request.POST, request.FILES)
        if context['form'].is_valid() and request.POST.get("upload", None):
            new_statements = []
            type = StatementType.objects.get(pk=1)
            account = Account.get_account_by_id(account_id)
            for record in process_records(account, request.FILES['file']):
                new_statements.append((record['amount'], record['date'], record['text'], type))
            Statement.insert_multiple_statements(account, new_statements)
            return HttpResponseRedirect(reverse('bankbook:account_summary',
                                                kwargs={'account_type': 'a', 'account_id': account_id}))
    else:
        context['form'] = UploadFileForm()
    return HttpResponse(template.render(context, request))
