from collections import OrderedDict
from itertools import groupby
from decimal import Decimal
import datetime
import statistics
import time


def decimal_default(obj):
    if isinstance(obj, Decimal) or isinstance(obj, int):
        return float(obj)
    else:
        raise TypeError


def add_missing_month_entries(data, min_month, max_month):
    num_month = max_month - min_month + 1
    for k in data:
        if data[k]:
            if type(data[k]['balance']) is list and len(data[k]['balance']) < num_month:
                data[k]['balance'] = data[k]['balance'] + [0]*(num_month - len(data[k]['balance']))


def discrete_bar_plot(summary, use_excluded=False, sort_by_label_name=True):
    chartData = {}
    for s in summary:
        name = s.statement_type.name
        if name in chartData:
            if use_excluded:
                chartData[name] = chartData[name] + s.excluded_balance
            else:
                chartData[name] = chartData[name] + s.balance
        else:
            if use_excluded:
                chartData[name] = s.excluded_balance
            else:
                chartData[name] = s.balance

    if sort_by_label_name:
        chartData = OrderedDict(sorted([(k, v) for k, v in chartData.items()], key=lambda tup: tup[1]))
    chart = {'key': 'Type result', 'values': []}
    for name in chartData:
        chart['values'].append({"label": name, "value": decimal_default(chartData[name])})
    return [chart]


def stacked_area_chart(summary_list, year=False):
    def month_resolution(summary_data):
        data = []
        for s in summary_data:
            timestamp = time.mktime(datetime.date(year=s.year, month=s.month, day=1).timetuple())*1000
            data.append([timestamp, decimal_default(s.balance)])
        return data

    def year_resolution(summary_data):
        data = []
        for s in summary_data:
            timestamp = time.mktime(datetime.date(year=s.year, month=1, day=1).timetuple())*1000
            data.append([timestamp, decimal_default(s.balance)])
        return data
    chart_data = []
    data_format_func = year_resolution if year else month_resolution
    summary_list = iter(groupby(sorted(summary_list, key=lambda s: s.account.id), lambda s: s.account))
    for account, summary in summary_list:
        data = data_format_func(summary)
        chart_data.append({'key': account.name, 'values': sorted(data, key=lambda x: x[0])})

    return chart_data


def discrete_bar_plot_mean(summary, min_month=-1, max_month=-1, use_exclude=False, sort_by_label_name=True):
    chartData = {}
    num_month = max_month - min_month
    for s in summary:
        if s.statement_type.name in chartData:
            if s.month in chartData[s.statement_type.name]:
                if use_exclude:
                    chartData[s.statement_type.name][s.month] += s.excluded_balance
                else:
                    chartData[s.statement_type.name][s.month] += s.balance
            else:
                if use_exclude:
                    chartData[s.statement_type.name][s.month] = s.excluded_balance
                else:
                    chartData[s.statement_type.name][s.month] = s.balance
        else:
            if use_exclude:
                chartData[s.statement_type.name] = {s.month: s.excluded_balance}
            else:
                chartData[s.statement_type.name] = {s.month: s.balance}
    chartData = {name: [chartData[name][m] for m in chartData[name]] for name in chartData}
    if sort_by_label_name:
        chartData = OrderedDict(sorted([(k, v) for k, v in chartData.items()], key=lambda tup: tup[1]))
    chart = {'key': 'Type result', 'values': []}
    biggest_expense = 0
    for name in chartData:
        if len(chartData[name]) < num_month:
            for i in range(num_month - len(chartData[name])):
                chartData[name].append(0)
        stdev = 0
        mean = decimal_default(statistics.mean(chartData[name]))
        if len(chartData[name]) > 1:
            stdev = abs(decimal_default(statistics.stdev(chartData[name])))
        if abs(mean) + stdev > biggest_expense:
            biggest_expense = abs(mean) + stdev
        chart['values'].append({"label": name, "value": mean, 'stdev': stdev})
        chart['biggest'] = biggest_expense * 1.05
    return [chart]


def create_date_range_and_populate(data, label, min_month=0, max_month=0):
    month_data = {'key': label, 'values': [{'x': str(month), 'y': 0.0} for month in range(min_month, max_month + 1)]}
    # month_data_all = {'key': label, 'values': [{'x': str(month), 'y': 0.0} for month in range(min_month, max_month + 1)]}
    for key in data:
        if label in data[key]:
            month_data['values'][int(key) - min_month]['y'] = data[key][label]['excluded_balance']
    #        month_data_all['values'][int(key) - min_month]['y'] = decimal_default(data[key][label]['balance'])
    return month_data  # , month_data_all)


def piechart(entries):
    data = [[], []]
    for d in entries:
        data[0].append({"label": d.statement_type.name, "value": decimal_default(d.balance)})
        data[1].append({"label": d.statement_type.name, "value": decimal_default(d.excluded_balance)})
    return data


def discrete_bar_plot_mean(summary,
                           min_month=-1,
                           max_month=-1,
                           use_exclude=False,
                           sort_by_label_name=False,
                           sort_by_amount=False):
    chartData = {}
    num_month = max_month - min_month
    for s in summary:
        if s.statement_type.name in chartData:
            if s.month in chartData[s.statement_type.name]:
                if use_exclude:
                    chartData[s.statement_type.name][s.month] += s.excluded_balance
                else:
                    chartData[s.statement_type.name][s.month] += s.balance
            else:
                if use_exclude:
                    chartData[s.statement_type.name][s.month] = s.excluded_balance
                else:
                    chartData[s.statement_type.name][s.month] = s.balance
        else:
            if use_exclude:
                chartData[s.statement_type.name] = {s.month: s.excluded_balance}
            else:
                chartData[s.statement_type.name] = {s.month: s.balance}
    chartData = {name: [chartData[name][m] for m in chartData[name]] for name in chartData}
    if sort_by_label_name:
        chartData = OrderedDict(sorted([(k, v) for k, v in chartData.items()], key=lambda tup: tup[0]))
    if sort_by_amount:
        chartData = OrderedDict(sorted([(k, v) for k, v in chartData.items()], key=lambda tup: sum(tup[1])))
    chart = {'key': 'Type result', 'values': []}
    biggest_expense = 0
    for name in chartData:
        if len(chartData[name]) < num_month:
            print((num_month - len(chartData[name]) + 1))
            chartData[name] = chartData[name] + [0] * (num_month - len(chartData[name]) + 1)
        stdev = 0
        print(name + " " + str(chartData[name]))
        mean = decimal_default(statistics.mean(chartData[name]))
        if len(chartData[name]) > 1:
            stdev = abs(decimal_default(statistics.stdev(chartData[name])))
        if abs(mean) + stdev > biggest_expense:
            biggest_expense = abs(mean) + stdev
        chart['values'].append({"label": name, "value": mean, 'stdev': stdev})
        chart['biggest'] = biggest_expense*1.05
    return [chart]
