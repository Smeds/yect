from bankbook.models import Account, Bank, Statement, StatementType
from decimal import Decimal
from distutils.util import strtobool
import datetime
import pandas as pd

from django.contrib.auth.models import User
from django.core.files.uploadedfile import UploadedFile
from ..forms import StatementFormSet


def export_statements_data():
    yield ("##User export")
    yield (";".join(['#id', "username", "first_name", "last_name", "email"]))
    for user in User.objects.all():
        yield (";".join([
            str(user.id),
            user.username,
            user.first_name,
            user.last_name,
            user.email
        ]))
    yield ("##Bank")
    yield (";".join(["#id", 'name', 'description']))
    for bank in Bank.objects.all():
        yield ";".join(
            [
                str(bank.id),
                bank.name,
                bank.description
            ])
    yield ("##Statement_type")
    yield (";".join(["#id", 'name', 'description', 'parent_type', 'sort_order']))
    for statement_type in StatementType.objects.all():
        yield ";".join(
            [
                str(statement_type.id),
                statement_type.name,
                statement_type.description,
                str(statement_type.parent_type),
                str(statement_type.sort_order)
            ])
    yield ("##Accounts")
    yield (";".join(["#id", "#bank", 'account_number', 'name', 'start_balance', 'owner']))
    for account in Account.objects.all():
        yield ";".join(
            [
                str(account.id),
                str(account.bank.id),
                account.account_number,
                account.name,
                str(account.start_balance),
                str(account.owner.username)
            ])
    yield ("##Statement")
    yield (";".join(["#id", 'account', 'amount', 'exclude', 'date', "text", "type"]))
    for statement in Statement.objects.all():
        yield ";".join(
            [
                str(statement.id),
                str(statement.account.id),
                str(statement.amount),
                str(statement.exclude),
                str(statement.date),
                statement.text,
                str(statement.type.id)
            ])


BANK_MAPPER = {'icabanken': 1, 'handelsbanken': 2, 'avanzabanken': 3}

COLUMN_MAPPER = {
    BANK_MAPPER['icabanken']: {'konto': {'amount': 'Belopp', 'date': 0, 'text': 'Text'}},
    BANK_MAPPER['avanzabanken']: {'konto': {'amount': 'Belopp', 'date': 'Datum', 'text': 'Värdepapper/beskrivning'}},
    BANK_MAPPER['handelsbanken']: {'allkort': {'amount': 'Belopp i SEK', 'date': 'Köpdatum', "text": 'Inköpsställe'},
                                   'allkonto': {'amount': 'Belopp', "date": 'Transaktionsdatum', "text": 'Text'}}
}

csv_header_bank = ['id', 'name', 'description']

csv_header_statement_type = ['id', 'name', 'description', 'parent_type', 'sort_order']

csv_header_account = ["id", "bank", 'account_number', 'name', 'start_balance', 'owner']

csv_header_statement = ["id", 'account', 'amount', 'exclude', 'date', "text", "type"]


def import_statements_data(file_handle):

    def compare_bank(bank, columns, header_map):
        if bank.id == int(columns[header_map['id']]) and \
           bank.name == columns[header_map['name']] and \
           bank.description == columns[header_map['description']]:
            pass
        else:
            raise Exception("Inconsistent bank information:, found bank " +
                            bank.name + " with id " +
                            columns[header_map['id']] +
                            ", trying to import bank with name " +
                            columns[header_map['name']] +
                            ", description " +
                            columns[header_map['description']])

    def compare_statement_type(statement_type, columns, header_map):
        if statement_type.id == int(columns[header_map['id']]) and \
           statement_type.name == columns[header_map['name']] and \
           statement_type.description == columns[header_map['description']] and \
           str(statement_type.parent_type) == columns[header_map['parent_type']] and \
           statement_type.sort_order == int(columns[header_map['sort_order']]):
            pass
        else:
            raise Exception("Inconsistent statement type information:, found statement type " +
                            statement_type.name + " with id " + columns[header_map['id']] +
                            ", trying to import statement type with name " + columns[header_map['name']] +
                            ", description " + columns[header_map['description']] +
                            ", parent_type " + columns[header_map['parent_type']] +
                            ", sort_order " + columns[header_map['sort_order']])

    def compare_account(account, columns, header_map):
        if account.id == int(columns[header_map['id']]) and \
           account.bank.id == int(columns[header_map['bank']]) and \
           account.account_number == columns[header_map['account_number']] and \
           account.name == columns[header_map['name']] and \
           account.start_balance == Decimal(columns[header_map['start_balance']]) and \
           account.owner.username == columns[header_map['owner']]:
            pass
        else:
            raise Exception("Inconsistent Account information:, found account " +
                            account.name + " with id " + columns[header_map['id']] +
                            ", trying to import account with name " + columns[header_map['name']] +
                            ", account number " + columns[header_map['account_number']] +
                            ", start_balance " + columns[header_map['start_balance']] +
                            ", owner " + columns[header_map['owner']])

    def compare_statement(statement, columns, header_map):
        if statement.id == int(columns[header_map['id']]) and \
           statement.account.id == int(columns[header_map['account']]) and \
           statement.amount == Decimal(columns[header_map['amount']]) and \
           statement.exclude == strtobool(columns[header_map['exclude']]) and \
           statement.date == datetime.datetime.strptime(columns[header_map['date']], "%Y-%m-%d").date() and \
           statement.text == columns[header_map['text']] and \
           statement.type.id == int(columns[header_map['type']]):
            pass
        else:
            raise Exception("Inconsistent statement information:, found statement " +
                            statement.text + " with id " + columns[header_map['id']] +
                            ", trying to import statement with " + columns[header_map['id']] +
                            ", account " + columns[header_map['account']] +
                            ", amount " + columns[header_map['amount']] +
                            ", exclude " + columns[header_map['exclude']] +
                            ", date " + columns[header_map['date']] +
                            ", text " + columns[header_map['text']] +
                            ", statement type " + columns[header_map['type']])
    line = next(file_handle)
    if not line.startswith("##User export"):
        raise Exception("Missing user information at start of file")
    line = next(file_handle)
    user_information = {}
    for line in file_handle:
        if line.startswith("#"):
            break
        line = line.rstrip("\n")
        columns = line.split(";")
        user = User.objects.filter(id=int(columns[0]))
        if len(user) > 0:
            user = user[0]
            if user.username != columns[1]:
                raise Exception("Inconsistent user information:, found user " +
                                user.username + " with id " + columns[0] +
                                ", trying to import user with username " + columns[1] + " with id " + columns[0])
        else:
            user = User.objects.create(id=int(columns[0]), username=columns[1], first_name=columns[2],
                                       last_name=columns[3])
        user_information[user.username] = user
    if not line.startswith("##Bank"):
        raise Exception("Bank information expected, found: " + line)
    line = next(file_handle)

    header_map = {key: value for key, value in zip(csv_header_bank, range(0, len(csv_header_bank)))}
    bank_information = {}
    for line in file_handle:
        if line.startswith("#"):
            break
        line = line.rstrip("\n")
        line = line.lstrip("\"").rstrip('"')
        columns = line.split(";")
        bank = Bank.objects.filter(id=columns[header_map['id']])
        if len(bank) > 0:
            compare_bank(bank[0], columns, header_map)
            bank_information[columns[header_map['id']]] = bank[0]
        else:
            bank_information[columns[header_map['id']]] = Bank.objects.create(
                id=int(columns[header_map['id']]),
                name=columns[header_map['name']],
                description=columns[header_map['description']])

    if not line.startswith("##Statement_type"):
        raise Exception("Statement type expected, found: " + line)

    line = next(file_handle)
    if line == "#" + ";".join(csv_header_statement_type):
        raise Exception("Statement type header mismatch, found: " +
                        line +
                        ",  expected " +
                        "#" +
                        ";".join(csv_header_statement_type))

    header_map = {key: value for key, value in zip(csv_header_statement_type, range(0, len(csv_header_statement_type)))}
    statement_type_information = {}
    for line in file_handle:
        if line.startswith("#"):
            break
        line = line.rstrip("\n")
        line = line.lstrip("\"").rstrip('"')
        columns = line.split(";")
        statement_type = StatementType.objects.filter(id=columns[header_map['id']])
        if len(statement_type) > 0:
            compare_statement_type(statement_type[0], columns, header_map)
            statement_type_information[columns[header_map['id']]] = statement_type[0]
        else:
            parent = None
            if columns[header_map['parent_type']] != 'None':
                parent = statement_type_information[columns[header_map['parent_type']]]
            statement_type_information[columns[header_map['id']]] = StatementType.objects.create(
                id=columns[header_map['id']],
                name=columns[header_map['name']],
                description=columns[header_map['description']],
                parent_type=parent,
                sort_order=columns[header_map['sort_order']])

    if not line.startswith("##Account"):
        raise Exception("Account expected, found: " + line)

    line = next(file_handle)
    if line == "#" + ";".join(csv_header_account):
        raise Exception("Account header mismatch, found: " + line + ",  expected " + "#" + ";".join(csv_header_account))

    header_map = {key: value for key, value in zip(csv_header_account, range(0, len(csv_header_account)))}
    account_information = {}

    for line in file_handle:
        if line.startswith("#"):
            break
        line = line.rstrip("\n")
        line = line.lstrip("\"").rstrip('"')
        columns = line.split(";")
        account = Account.objects.filter(id=columns[header_map['id']])
        if len(account) > 0:
            compare_account(account[0], columns, header_map)
            account_information[columns[header_map['id']]] = account[0]
        else:
            account_information[columns[header_map['id']]] = Account.objects.create(
                id=columns[header_map['id']],
                account_number=columns[header_map['account_number']],
                start_balance=Decimal(columns[header_map['start_balance']]),
                name=columns[header_map['name']],
                bank=bank_information[columns[header_map['bank']]],
                owner=user_information[columns[header_map['owner']]])

    if not line.startswith("##Statement"):
        raise Exception("Statement expected, found: " + line)

    line = next(file_handle)
    if line == "#" + ";".join(csv_header_statement):
        raise Exception("Statement header mismatch, found: " + line + ",  expected " + "#" + ";".join(csv_header_statement))

    header_map = {key: value for key, value in zip(csv_header_statement, range(0, len(csv_header_statement)))}
    import datetime
    for line in file_handle:
        if line.startswith("#"):
            break
        line = line.rstrip("\n")
        line = line.lstrip("\"").rstrip('"')
        columns = line.split(";")
        statement = Statement.objects.filter(id=columns[header_map['id']])
        if len(statement) > 0:
            compare_statement(statement[0], columns, header_map)
        else:
            statement = Statement.objects.create(
                id=columns[header_map['id']],
                account=account_information[columns[header_map['account']]],
                amount=Decimal(columns[header_map['amount']]),
                exclude=strtobool(columns[header_map['exclude']]),
                date=datetime.datetime.strptime(columns[header_map['date']], "%Y-%m-%d").date(),
                text=columns[header_map['text']],
                type=statement_type_information[columns[header_map['type']]])


#  REMOVE
def process_uploaded_statement(request, file, account):
    return StatementFormSet(initial=process_records(account, file))


def process_records(account, file):
    if file.name.endswith(".csv"):
        return process_csv_file(account, file)
    elif file.name.endswith(".xls"):
        return process_xls_file(account, file)
    else:
        raise Exception("Unknown filetype %s!" % file.name)


def process_xls_file(account, file):
    encoding = 'UTF'
    if not isinstance(file, UploadedFile):
        encoding = get_encoding(file).decode('UTF-8')
    xls_file = pd.read_html(file, header=0, thousands=" ")
    if BANK_MAPPER['handelsbanken'] == account.bank.pk:
        return process_handelsbanken_xls(account, xls_file)


def extract_data(data, account, type, number_formater, date_formater):
    date = date_formater(data[COLUMN_MAPPER[account.bank.pk][type]['date']])
    text = data[COLUMN_MAPPER[account.bank.pk][type]['text']]
    amount = number_formater(data[COLUMN_MAPPER[account.bank.pk][type]['amount']])
    return {
            'date': date,
            'text': text,
            'transaction_type': "",
            'amount': amount}


def process_csv_file(account, file_csv):
    encoding = 'UTF'
    if not isinstance(file_csv, UploadedFile):
        encoding = get_encoding(file_csv).decode('UTF-8')
    csv_file = pd.read_csv(file_csv)
    newStatements = []
    if len(csv_file) > 0:
        for i, value in csv_file.iterrows():
            newStatements.append(
                extract_data(value,
                             account,
                             'konto',
                             lambda x: Decimal(x.replace(',', '.').replace(' ', '').replace('kr', '')),
                             lambda x: datetime.datetime.strptime(x, "%Y-%m-%d")))
    else:
        raise Exception("File format unknown 4")
    return newStatements


def process_handelsbanken_xls(account, xls_file):
    newStatements = []
    if len(xls_file) > 0:
        if xls_file[0].size > 0:
            if xls_file[0].iloc[0][1].startswith("ALLKORT"):
                for i, value in xls_file[1].iterrows():
                    newStatements.append(
                        extract_data(value,
                                     account,
                                     'allkort',
                                     lambda x: Decimal(x.replace(',', '.').replace(' ', '')),
                                     lambda x: datetime.datetime.strptime(x, "%Y-%m-%d")))

            else:
                raise Exception("File format unknown 1")
        elif xls_file[0].size == 0:
            if xls_file[0].columns[1].startswith("Allkortskonto") or xls_file[0].columns[1].startswith("Allkonto"):
                for i, value in xls_file[3].iterrows():
                    newStatements.append(
                        extract_data(value,
                                     account,
                                     'allkonto',
                                     lambda x: Decimal(x.replace(',', '.').replace(' ', '')),
                                     lambda x: datetime.datetime.strptime(x, "%Y-%m-%d")))
            else:
                raise Exception("File format unknown 2")
        else:
            raise Exception("File format unknown 3")
    else:
        raise Exception("File format unknown 4")
    return newStatements
