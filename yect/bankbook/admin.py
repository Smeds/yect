from django.contrib import admin
from django.core.files.uploadedfile import UploadedFile
from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from django.urls import path

import pandas as pd
import datetime
from decimal import Decimal
from django.shortcuts import redirect

from .forms import UploadFileForm
from .models import Account, CombinedAccount, Bank, Statement, StatementType
from .models import YearSummaryCumulative, MonthSummaryCumulative
from .models import CombinedYearSummaryCumulative, CombinedMonthSummaryCumulative
from .models import StatementTypeYearSummary, StatementTypeMonthSummary

from libs.files.utils import get_encoding


def open_csv_file(file):
    encoding = 'utf-8'
    if not isinstance(file, UploadedFile):
        encoding = get_encoding(file)
    return pd.read_csv(file, index_col=False, encoding=encoding, sep=";",
                       parse_dates=False)


class StatementModelAdmin(admin.ModelAdmin):
    def purge_statements(self, request):
        YearSummaryCumulative.objects.all().delete()
        MonthSummaryCumulative.objects.all().delete()
        StatementTypeYearSummary.objects.all().delete()
        StatementTypeMonthSummary.objects.all().delete()
        Statement.objects.all().delete()
        return redirect("admin:bankbook_statement_changelist")

    def upload_statements(self, request):
        context = {}
        if request.method == 'POST':
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                data = open_csv_file(request.FILES['file'])
                statements = {}
                for index, row in data.iterrows():
                    statement_id, date, text, amount, account, type, exclude = row
                    if account in statements:
                        statements[account]['statements'].append((
                            Decimal(amount),
                            datetime.datetime.strptime(date, "%Y-%m-%d"),
                            text,
                            StatementType.objects.get(id=type)))
                    else:
                        statements[account] = {
                            'account': Account.objects.get(id=account),
                            'statements': [(
                                           Decimal(amount),
                                           datetime.datetime.strptime(date, "%Y-%m-%d"),
                                           text,
                                           StatementType.objects.get(id=type))]}
                print("Loaded statements")
                for account in statements:
                    print("-- Inserting statements")
                    Statement.insert_multiple_statements(statements[account]['account'], statements[account]['statements'])
                return redirect("admin:bankbook_statement_changelist")
            else:
                context["form"] = form
        else:
            context["form"] = UploadFileForm()
        return TemplateResponse(request, "admin/bankbook/statement/upload.html", context)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [path("upload/", self.upload_statements), path("purge/", self.purge_statements)]
        return my_urls + urls


class AccountModelAdmin(admin.ModelAdmin):
    def upload_accounts(self, request):
        context = {}
        if request.method == 'POST':
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                data = open_csv_file(request.FILES['file'])
                for index, row in data.iterrows():
                    account_id, account_number, name, start_balance, bank, owner = row
                    account = Account(id=int(account_id),
                                      bank=Bank.objects.get(id=bank),
                                      account_number=account_number,
                                      name=name,
                                      start_balance=start_balance,
                                      owner=User.objects.get(id=owner))
                    account.save()
                return redirect("admin:bankbook_account_changelist")
            else:
                context["form"] = form
        else:
            context["form"] = UploadFileForm()
        return TemplateResponse(request, "admin/bankbook/account/upload.html", context)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [path("upload/", self.upload_accounts)]
        return my_urls + urls


admin.site.register(Account, AccountModelAdmin)
admin.site.register(CombinedAccount)
admin.site.register(Bank)
admin.site.register(Statement, StatementModelAdmin)
admin.site.register(StatementType)

admin.site.register(YearSummaryCumulative)
admin.site.register(MonthSummaryCumulative)
admin.site.register(CombinedYearSummaryCumulative)
admin.site.register(CombinedMonthSummaryCumulative)
admin.site.register(StatementTypeYearSummary)
admin.site.register(StatementTypeMonthSummary)


def export_flowcell_data(self, request):
    from .utils.data import export_flowcell_statements_data
    response = HttpResponse(content_type='text/csv')
    from datetime import datetime
    response['Content-Disposition'] = 'attachment; filename="yect_export.' + \
                                      datetime.today().strftime('%Y-%m-%d') + '.csv"'
    writer = csv.writer(response)
    for data in export_flowcell_statements_data():
        writer.writerow([data])
    return response
