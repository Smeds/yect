from django import forms

from .models import Account, Bank, CombinedAccount, CombineItem, Statement, StatementType


class UploadFileForm(forms.Form):
    file = forms.FileField()


class AccountForm(forms.ModelForm):
    bank = forms.ModelChoiceField(label="select a bank",
                                  queryset=Bank.objects.all(),
                                  widget=forms.Select())
    owner = forms.HiddenInput()

    class Meta:
        model = Account
        fields = ['bank', 'account_number', 'name', 'start_balance', 'owner']
        widgets = {
            'account_number': forms.TextInput(),
            'name': forms.TextInput(),
            'start_balance': forms.TextInput(),
            'owner': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        owner = kwargs.pop('owner', None)
        super(AccountForm, self).__init__(*args, **kwargs)
        if owner:
            self.fields['owner'].initial = str(owner.id)


class BaseCombineItemFormSet(forms.BaseFormSet):
    def clean(self):
        """
        Validates that the same account haven't been included multiple times
        :return:
        """
        if any(self.errors):
            return
        accounts = []
        duplicates = False
        for form in self.forms:
            if form.cleaned_data:
                account = form.cleaned_data['account']
                if account:
                    if account in accounts:
                        duplicates = True
                    accounts.append(account)
                if duplicates:
                    raise forms.ValidationError("The same account cannot be included more than once.")
                if not account:
                    raise forms.ValidationError("An account selection cannot be empty.")
        if len(accounts) < 2:
            raise forms.ValidationError("A minimum of two accounts have to be selected.")


class CombinedAccountForm(forms.ModelForm):
    class Meta:
        model = CombinedAccount
        fields = ['owner', 'name']
        widgets = {
            'owner': forms.HiddenInput(),
            'name': forms.TextInput(),
        }

    def __init__(self, *args, **kwargs):
        owner = kwargs.pop('owner', None)
        super(CombinedAccountForm, self).__init__(*args, **kwargs)
        if owner:
            self.fields['owner'].initial = str(owner.pk)


class CombinedItemForm(forms.ModelForm):
    class Meta:
        model = CombineItem
        fields = ['account']
        widgets = {
            'account': forms.Select(attrs={'class': 'input-field'}),
        }

    def __init__(self, *args, **kwargs):
        owner = kwargs.pop('owner', None)
        super(CombinedItemForm, self).__init__(*args, **kwargs)
        if owner:
            self.fields['account'].queryset = CombineItem.get_accounts(owner)

    def create_combine_item_form_set(extra=0):
        return forms.formset_factory(CombinedItemForm, formset=BaseCombineItemFormSet, extra=extra)


class StatementForm(forms.ModelForm):
    editable = True
    type = forms.ModelChoiceField(
        label="select a statement type",
        queryset=StatementType.objects.all().order_by('sort_order'),
        widget=forms.Select(attrs={'class': "custom-form-select"}))

    class Meta:
        model = Statement
        fields = ['date', 'text', 'amount', 'account', 'type', 'exclude']
        exclude = ['transaction_type', 'group', ]
        widgets = {
            'date': forms.TextInput(attrs={'class': 'form-control col-md-7 col-xs-12', 'readonly': 'readonly'}),
            'text': forms.TextInput(attrs={'class': 'form-control col-md-7 col-xs-12', 'readonly': 'readonly'}),
            'transaction_type': forms.TextInput(attrs={'class': 'form-control col-md-7 col-xs-12', 'readonly': 'readonly'}),
            'group': forms.TextInput(attrs={'class': 'form-control col-md-7 col-xs-12', 'readonly': 'readonly'}),
            'amount': forms.TextInput(attrs={'class': 'form-control col-md-7 col-xs-12', 'readonly': 'readonly'}),
            'account': forms.TextInput(attrs={'class': 'custom-form-select'}),
            'type': forms.Select(attrs={'class': "custom-form-select"}),
            'exclude': forms.CheckboxInput(),
        }


class StatementTypeForm(forms.ModelForm):
    class Meta:
        model = StatementType
        fields = ['name', 'description', 'parent_type']
        exclude = ['description']


class NewStatementForm(forms.ModelForm):
    type = forms.ModelChoiceField(
        initial=lambda: StatementType.objects.get(name="Unknown"),
        label="select a statement type",
        queryset=StatementType.objects.order_by('sort_order').all(),
        widget=forms.Select(attrs={'class': "custom-form-select"}))

    class Meta:
        model = Statement
        fields = ['date', 'text', 'amount', 'account', 'type', 'exclude']
        exclude = ['transaction_type', 'group']
        widgets = {
            'date': forms.DateInput(format='%Y-%m-%d',
                                    attrs={'class': 'form-control',
                                           'style': 'width:100px',
                                           'readonly': 'readonly'}),
            'text': forms.TextInput(attrs={'class': 'form-control',
                                           'readonly': 'readonly',
                                           'style': 'width:100%'}),
            'transaction_type': forms.HiddenInput(),
            'group': forms.HiddenInput(),
            'amount': forms.TextInput(attrs={'class': 'form-control',
                                             'readonly': 'readonly',
                                             'style': 'width:100px'}),
            'account': forms.HiddenInput(),
            'type': forms.TextInput(attrs={'class': "custom-form-select"}),
            'exclude': forms.CheckboxInput(attrs={'class': 'form-control col-md-7 col-xs-12'}),
        }


StatementFormSet = forms.formset_factory(NewStatementForm, extra=0)


class UploadFileForm(forms.Form):
    file = forms.FileField(widget=forms.FileInput(), label="Browse")
