function multiChartPlot(data,id) {
    var margin = {top: 30, right: 20, bottom: 30, left: 40};
    nv.addGraph(function() {
        var extent = d3.extent(function(array){
        values = []
        array.forEach(function(item){
                item['values'].forEach(function(value){
                    values.push(value['y']);
                });
            });
            return(values);
        }(data));
        var chart = nv.models.multiBarChart();
        chart.yAxis.tickFormat(d3.format(',.0f'));
        chart.reduceXTicks(false);
        chart.groupSpacing(0.5);
        d3.select(id) 
            .datum(data)
            .call(chart);
        return chart;
    });
};