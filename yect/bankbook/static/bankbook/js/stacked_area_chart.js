function stackedAreaChart(data,id, dateformat) {
    nv.addGraph(function() { 
        var chart;
        chart = nv.models.stackedAreaChart() 
                .margin({left: 100}) 
                .x(function(d) { return d[0] })
                .y(function(d) { return d[1] })
                .useInteractiveGuideline(true)
                .rightAlignYAxis(false)
                .showControls(false)
                .clipEdge(true);  
        chart.xAxis
        .tickFormat(function(d) {
          return d3.time.format(dateformat)(new Date(d))});
        chart.yAxis 
            .tickFormat(d3.format(',.2f'));  
        d3.select(id) 
            .datum(data)
        .call(chart);
        nv.utils.windowResize(chart.update);
        return chart;
    }); 
};