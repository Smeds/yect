function linechart(data,id,xaxis_label) {
    nv.addGraph(function() {
        var chart = nv.models.lineChart()
            .x(function(d) { return d.x })    //Specify the data accessors.
            .y(function(d) { return d.y })
            .showLegend(false)
            .showYAxis(true)
            .showXAxis(true);

        chart.xAxis     //Chart y-axis settings
            .axisLabel(xaxis_label)

        chart.yAxis     //Chart y-axis settings
            .axisLabel('Amount').tickFormat(d3.format(',.0f'));


        d3.select(id)
          .datum(data)
            .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
  });
};