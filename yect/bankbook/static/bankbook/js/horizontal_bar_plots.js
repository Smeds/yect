function createHorizontal(data,id) {
    nv.addGraph(function() {
        var chart = nv.models.multiBarHorizontalChart()
            .x(function(d) { return d.label })    //Specify the data accessors.
            .y(function(d) { return d.value })
            .margin({left: 150})
            .showLegend(false)
            .barColor(d3.scale.category20().range())
            .showControls(false);
             d3.select(id)
                 .datum(data)
                     .call(chart);

             nv.utils.windowResize(chart.update);

        return chart;
    });
};

function createHorizontalErrorBar(data,biggest,id) {
    nv.addGraph(function() {
        var chart = nv.models.multiBarHorizontalChart()
            .x(function(d) { return d.label })    //Specify the data accessors.
            .y(function(d) { return d.value })
            .forceY([-biggest,0])
            .yErr(function(d) { return [-d.stdev,d.stdev]})
            .margin({left: 150})
            .showLegend(false)
            .barColor(d3.scale.category20().range())
            .showControls(false);
            d3.select(id)
                .datum(data)
                    .call(chart);

            nv.utils.windowResize(chart.update);

            return chart;
    });
};