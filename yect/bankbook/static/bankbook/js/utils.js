function addMissingTimeEvents(data) {
    var min_index = 0;
    var max_index = 0
    for (var i = 1; i < data.length; i++){
        if (data[i]['values'][0][0] < data[min_index]['values'][0][0]) {
            min_index = i
        }
        if (data[i]['values'][data[i]['values'].length - 1][0] > data[max_index]['values'][data[max_index]['values'].length-1][0]) {
            max_index = i
        }
    }
    for (var i = 0; i < data.length; i++){
        if  (i != min_index){
            var new_before_data = [];
            for (var j = 0; j < data[min_index]['values'].length; j++){
                if (data[min_index]['values'][j][0] < data[i]['values'][0][0]){
                    new_before_data.push([data[min_index]['values'][j][0],0]);
                } else { break; }

            }
            if(new_before_data.length > 0) {data[i]['values'] = new_before_data.concat(data[i]['values']);}
        }
        if  (i != max_index){
            var new_after_data = [];
            for (var j = data[max_index]['values'].length -1; 0 < j; j--){
                if (data[i]['values'][data[i]['values'].length - 1 ][0] < data[max_index]['values'][j][0]){
                    new_after_data.unshift([data[max_index]['values'][j][0],data[i]['values'][data[i]['values'].length -1][1]]);
                } else { break; }

            }
            if(new_after_data.length > 0) {data[i]['values'] = data[i]['values'].concat(new_after_data);}
        }
    }
    return data
}