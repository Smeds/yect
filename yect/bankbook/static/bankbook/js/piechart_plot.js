function createPiechart(data,id) {
    nv.addGraph(function()  {
        var chart = nv.models.pieChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .labelType("percent")
            .showLabels(true)
            .donut(true)
            .donutRatio(0.35);

            d3.select(id)
                .datum(data)
                .transition().duration(350)
                .call(chart);

            return chart;
    });
};