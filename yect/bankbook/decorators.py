from .models import Account, CombinedAccount, Statement
from django.core.exceptions import PermissionDenied


def validate_account_access(func):
    """
        Decorator used to validate that the user should be able to
        access the account.
    """
    def wrapper(request, *args, **kwargs):
        account_id = kwargs['account_id']
        account_type = kwargs['account_type']
        user = request.user
        if (account_type == "c" and CombinedAccount.objects.filter(owner=user, pk=account_id)) or \
           (account_type == "a" and Account.objects.filter(owner=user, pk=account_id)):
            return func(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrapper.__doc__ = func.__doc__
    wrapper.__name__ = func.__name__
    return wrapper


def validate_statement_access(func):
    """
        Decorator used to validate that the user should be able to
        access the satatement.
    """
    def wrapper(request, *args, **kwargs):
        statement = Statement.objects.get(pk=kwargs['statement_id'])
        user = request.user
        if statement.account.owner == user:
            return func(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrapper.__doc__ = func.__doc__
    wrapper.__name__ = func.__name__
    return wrapper
