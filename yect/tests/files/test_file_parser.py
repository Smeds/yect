import datetime
import unittest
from pathlib import Path
from libs.files.file_parser import parse_file
from decimal import Decimal


class MyTestCase(unittest.TestCase):
    def test_parse_ica(self):
        ica_data = parse_file(Path("tests/files/exports/ica_export.csv"))
        self.assertEqual(ica_data,
                         [(datetime.datetime(2015, 1, 1, 0, 0), 'City Gross Uppsala', Decimal('-1202.12')),
                          (datetime.datetime(2015, 1, 1, 0, 0), 'Systembolaget Bolander', Decimal('-613.60'))])

    def test_parse_handelsbanken_allkonto(self):
        handelsbanken_data = parse_file(Path("tests/files/exports/allkonto_export.xls"))
        self.assertEqual(handelsbanken_data,
                         [(datetime.datetime(2016, 7, 5, 0, 0), 'Fred Man', Decimal('-270.00')),
                          (datetime.datetime(2015, 4, 30, 0, 0), 'Periodens köp', Decimal('-5348.03'))])

        handelsbanken_data_real_xls = parse_file(Path("tests/files/exports/allkonto_export.real_xls.xls"))
        self.assertEqual(handelsbanken_data_real_xls,
                         [(datetime.datetime(2016, 7, 5, 0, 0), 'Fred Man', Decimal('-270.00')),
                          (datetime.datetime(2015, 4, 30, 0, 0), 'Periodens köp', Decimal('-5348.03'))])


if __name__ == '__main__':
    unittest.main()
